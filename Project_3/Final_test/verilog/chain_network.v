//
// Generated by Bluespec Compiler, version untagged-g5f8fb782 (build 5f8fb782)
//
// On Wed Jan  5 19:41:35 IST 2022
//
//
// Ports:
// Name                         I/O  size props
// the_answer                     O    32 const
// RDY_the_answer                 O     1 const
// CLK                            I     1 clock
// RST_N                          I     1 reset
//
// No combinational paths from inputs to outputs
//
//

`ifdef BSV_ASSIGNMENT_DELAY
`else
  `define BSV_ASSIGNMENT_DELAY
`endif

`ifdef BSV_POSITIVE_RESET
  `define BSV_RESET_VALUE 1'b1
  `define BSV_RESET_EDGE posedge
`else
  `define BSV_RESET_VALUE 1'b0
  `define BSV_RESET_EDGE negedge
`endif

module chain_network(CLK,
		     RST_N,

		     the_answer,
		     RDY_the_answer);
  input  CLK;
  input  RST_N;

  // value method the_answer
  output [31 : 0] the_answer;
  output RDY_the_answer;

  // signals for module outputs
  wire [31 : 0] the_answer;
  wire RDY_the_answer;

  // register arr_0
  reg [31 : 0] arr_0;
  wire [31 : 0] arr_0_D_IN;
  wire arr_0_EN;

  // register arr_1
  reg [31 : 0] arr_1;
  wire [31 : 0] arr_1_D_IN;
  wire arr_1_EN;

  // register arr_2
  reg [31 : 0] arr_2;
  wire [31 : 0] arr_2_D_IN;
  wire arr_2_EN;

  // register arr_3
  reg [31 : 0] arr_3;
  wire [31 : 0] arr_3_D_IN;
  wire arr_3_EN;

  // register arr_4
  reg [31 : 0] arr_4;
  wire [31 : 0] arr_4_D_IN;
  wire arr_4_EN;

  // register arr_5
  reg [31 : 0] arr_5;
  wire [31 : 0] arr_5_D_IN;
  wire arr_5_EN;

  // register arr_6
  reg [31 : 0] arr_6;
  wire [31 : 0] arr_6_D_IN;
  wire arr_6_EN;

  // register arr_7
  reg [31 : 0] arr_7;
  wire [31 : 0] arr_7_D_IN;
  wire arr_7_EN;

  // register arr_8
  reg [31 : 0] arr_8;
  wire [31 : 0] arr_8_D_IN;
  wire arr_8_EN;

  // register count
  reg [31 : 0] count;
  wire [31 : 0] count_D_IN;
  wire count_EN;

  // register destination
  reg [3 : 0] destination;
  wire [3 : 0] destination_D_IN;
  wire destination_EN;

  // register source
  reg [3 : 0] source;
  wire [3 : 0] source_D_IN;
  wire source_EN;

  // register temp
  reg [31 : 0] temp;
  wire [31 : 0] temp_D_IN;
  wire temp_EN;

  // rule scheduling signals
  wire CAN_FIRE_RL_load_array,
       CAN_FIRE_RL_load_array_1,
       CAN_FIRE_RL_load_array_2,
       CAN_FIRE_RL_load_array_3,
       CAN_FIRE_RL_load_array_4,
       CAN_FIRE_RL_load_array_5,
       CAN_FIRE_RL_load_array_6,
       CAN_FIRE_RL_load_array_7,
       CAN_FIRE_RL_load_array_8,
       CAN_FIRE_RL_print_chain_routing,
       CAN_FIRE_RL_print_chain_routing_1,
       CAN_FIRE_RL_print_chain_routing_2,
       CAN_FIRE_RL_space_print,
       WILL_FIRE_RL_load_array,
       WILL_FIRE_RL_load_array_1,
       WILL_FIRE_RL_load_array_2,
       WILL_FIRE_RL_load_array_3,
       WILL_FIRE_RL_load_array_4,
       WILL_FIRE_RL_load_array_5,
       WILL_FIRE_RL_load_array_6,
       WILL_FIRE_RL_load_array_7,
       WILL_FIRE_RL_load_array_8,
       WILL_FIRE_RL_print_chain_routing,
       WILL_FIRE_RL_print_chain_routing_1,
       WILL_FIRE_RL_print_chain_routing_2,
       WILL_FIRE_RL_space_print;

  // value method the_answer
  assign the_answer = 32'hAAAAAAAA ;
  assign RDY_the_answer = 1'd1 ;

  // rule RL_load_array
  assign CAN_FIRE_RL_load_array = 1'd1 ;
  assign WILL_FIRE_RL_load_array = 1'd1 ;

  // rule RL_load_array_1
  assign CAN_FIRE_RL_load_array_1 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_1 = 1'd1 ;

  // rule RL_load_array_2
  assign CAN_FIRE_RL_load_array_2 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_2 = 1'd1 ;

  // rule RL_load_array_6
  assign CAN_FIRE_RL_load_array_6 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_6 = 1'd1 ;

  // rule RL_load_array_7
  assign CAN_FIRE_RL_load_array_7 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_7 = 1'd1 ;

  // rule RL_load_array_8
  assign CAN_FIRE_RL_load_array_8 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_8 = 1'd1 ;

  // rule RL_print_chain_routing
  assign CAN_FIRE_RL_print_chain_routing = 1'd1 ;
  assign WILL_FIRE_RL_print_chain_routing = 1'd1 ;

  // rule RL_load_array_5
  assign CAN_FIRE_RL_load_array_5 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_5 = 1'd1 ;

  // rule RL_print_chain_routing_1
  assign CAN_FIRE_RL_print_chain_routing_1 = 1'd1 ;
  assign WILL_FIRE_RL_print_chain_routing_1 = 1'd1 ;

  // rule RL_load_array_4
  assign CAN_FIRE_RL_load_array_4 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_4 = 1'd1 ;

  // rule RL_print_chain_routing_2
  assign CAN_FIRE_RL_print_chain_routing_2 = 1'd1 ;
  assign WILL_FIRE_RL_print_chain_routing_2 = 1'd1 ;

  // rule RL_load_array_3
  assign CAN_FIRE_RL_load_array_3 = 1'd1 ;
  assign WILL_FIRE_RL_load_array_3 = 1'd1 ;

  // rule RL_space_print
  assign CAN_FIRE_RL_space_print = 1'd1 ;
  assign WILL_FIRE_RL_space_print = 1'd1 ;

  // register arr_0
  assign arr_0_D_IN = 32'd0 ;
  assign arr_0_EN = 1'd1 ;

  // register arr_1
  assign arr_1_D_IN = 32'd1 ;
  assign arr_1_EN = 1'd1 ;

  // register arr_2
  assign arr_2_D_IN = 32'd2 ;
  assign arr_2_EN = 1'd1 ;

  // register arr_3
  assign arr_3_D_IN = 32'd3 ;
  assign arr_3_EN = 1'd1 ;

  // register arr_4
  assign arr_4_D_IN = 32'd4 ;
  assign arr_4_EN = 1'd1 ;

  // register arr_5
  assign arr_5_D_IN = 32'd5 ;
  assign arr_5_EN = 1'd1 ;

  // register arr_6
  assign arr_6_D_IN = 32'd6 ;
  assign arr_6_EN = 1'd1 ;

  // register arr_7
  assign arr_7_D_IN = 32'd7 ;
  assign arr_7_EN = 1'd1 ;

  // register arr_8
  assign arr_8_D_IN = 32'd8 ;
  assign arr_8_EN = 1'd1 ;

  // register count
  assign count_D_IN = 32'h0 ;
  assign count_EN = 1'b0 ;

  // register destination
  assign destination_D_IN = 4'h0 ;
  assign destination_EN = 1'b0 ;

  // register source
  assign source_D_IN = 4'h0 ;
  assign source_EN = 1'b0 ;

  // register temp
  assign temp_D_IN = 32'h0 ;
  assign temp_EN = 1'b0 ;

  // handling of inlined registers

  always@(posedge CLK)
  begin
    if (RST_N == `BSV_RESET_VALUE)
      begin
        count <= `BSV_ASSIGNMENT_DELAY 32'd0;
	destination <= `BSV_ASSIGNMENT_DELAY 4'd0;
	source <= `BSV_ASSIGNMENT_DELAY 4'd0;
	temp <= `BSV_ASSIGNMENT_DELAY 32'd0;
      end
    else
      begin
        if (count_EN) count <= `BSV_ASSIGNMENT_DELAY count_D_IN;
	if (destination_EN)
	  destination <= `BSV_ASSIGNMENT_DELAY destination_D_IN;
	if (source_EN) source <= `BSV_ASSIGNMENT_DELAY source_D_IN;
	if (temp_EN) temp <= `BSV_ASSIGNMENT_DELAY temp_D_IN;
      end
    if (arr_0_EN) arr_0 <= `BSV_ASSIGNMENT_DELAY arr_0_D_IN;
    if (arr_1_EN) arr_1 <= `BSV_ASSIGNMENT_DELAY arr_1_D_IN;
    if (arr_2_EN) arr_2 <= `BSV_ASSIGNMENT_DELAY arr_2_D_IN;
    if (arr_3_EN) arr_3 <= `BSV_ASSIGNMENT_DELAY arr_3_D_IN;
    if (arr_4_EN) arr_4 <= `BSV_ASSIGNMENT_DELAY arr_4_D_IN;
    if (arr_5_EN) arr_5 <= `BSV_ASSIGNMENT_DELAY arr_5_D_IN;
    if (arr_6_EN) arr_6 <= `BSV_ASSIGNMENT_DELAY arr_6_D_IN;
    if (arr_7_EN) arr_7 <= `BSV_ASSIGNMENT_DELAY arr_7_D_IN;
    if (arr_8_EN) arr_8 <= `BSV_ASSIGNMENT_DELAY arr_8_D_IN;
  end

  // synopsys translate_off
  `ifdef BSV_NO_INITIAL_BLOCKS
  `else // not BSV_NO_INITIAL_BLOCKS
  initial
  begin
    arr_0 = 32'hAAAAAAAA;
    arr_1 = 32'hAAAAAAAA;
    arr_2 = 32'hAAAAAAAA;
    arr_3 = 32'hAAAAAAAA;
    arr_4 = 32'hAAAAAAAA;
    arr_5 = 32'hAAAAAAAA;
    arr_6 = 32'hAAAAAAAA;
    arr_7 = 32'hAAAAAAAA;
    arr_8 = 32'hAAAAAAAA;
    count = 32'hAAAAAAAA;
    destination = 4'hA;
    source = 4'hA;
    temp = 32'hAAAAAAAA;
  end
  `endif // BSV_NO_INITIAL_BLOCKS
  // synopsys translate_on

  // handling of system tasks

  // synopsys translate_off
  always@(negedge CLK)
  begin
    #0;
    $display("%d", $signed(arr_5));
    $display("%d", $signed(arr_4));
    $display("%d", $signed(arr_3));
    $display(" ");
  end
  // synopsys translate_on
endmodule  // chain_network

