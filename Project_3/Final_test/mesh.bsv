/***************************************************************************
*
* Project           		    :  PROJECT-3
* Name of the file	     	    :  mesh.bsv
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/


package  mesh;


(* synthesize *)
module mesh_network();

Integer row = 3;
Integer col = 4;
Int#(32) src = 4;
Int#(32) dest = 9;
// Reg#(int) source <- mkReg(0);
Int#(32) mesh[row][col];

function Bool checking( td i, td j ) provisos( Eq#(td) );
    return ( i == j);
endfunction

function td nop( td i );
return i;
endfunction

// Creating a mesh[row][col]
Integer count = 0;
for(Integer i = 0; i < row; i = i+1)
begin
    for(Integer j = 0; j < col; j = j+1)
    begin
        count = count + 1;
        mesh[i][j] = fromInteger(count);
    end
end

// Register a routing_mesh[row * col]
Reg#(Int#(32)) routing_mesh[row * col];
for(Integer j = 0; j < row*col; j = j+1)
    routing_mesh[j] <- mkRegU;


rule mesh_network_print;
    for(Integer i = 0; i < row; i = i+1)
        for(Integer j = 0; j < col; j = j+1)
            $display("mesh[%d][%d]: %d", i, j, mesh[i][j]);
endrule


rule head_node;
    Integer head_node_row = 0, head_node_col = 0;
    if(row == col)
    begin
        if(row % 2 == 0)
        begin
            head_node_row = row/2;
            head_node_col = col/2;
        end
        else
        begin
            head_node_row = (row+1)/2;
            head_node_col = (col+1)/2;
        end
    end
    else
    begin
        if(row % 2 == 0)
            head_node_row = row/2;
        else
            head_node_row = (row+1)/2;
        if(col % 2 == 0)
            head_node_col = col/2;
        else
            head_node_col = (col+1)/2;
    end

    $display("Head_Node:%d", mesh[head_node_row][head_node_col]);

endrule


rule mesh_routing;
    Integer src_row = 0, src_col = 0;
    Integer dest_row = 0, dest_col = 0;
    for(Integer i = 0; i < row; i = i+1)
    begin
        for(Integer j = 0; j < col ; j = j+1)
        begin
            if(src == mesh[i][j])
            begin
                src_row = i;
                src_col = j;
            end
            if(dest == mesh[i][j])
            begin
                dest_row = i;
                dest_col = j;
            end
        end
    end
    $display("src_row:%d, src_col:%d, dest_row:%d, dest_col:%d", src_row, src_col, dest_row, dest_col);


    //Finding the routing of Mesh
    Integer len = 0, deciding = 0, len_temp = 0;
    if(src_col <= dest_col) /*Source should be left side of the destination*/
    begin
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_mesh[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == mesh[i][src_col + k])  //Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_mesh[len] <= mesh[i][src_col + k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_mesh[len] <= mesh[src_row][src_col + k];  //X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            else
                $display(" nop is ", nop(10));
        end
    end
    else /*Source should be right side of the destination*/
    begin  
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_mesh[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == mesh[i][src_col - k])  //Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_mesh[len] <= mesh[i][src_col - k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_mesh[len] <= mesh[src_row][src_col - k];  //X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            else
                $display(" nop is ", nop(10));
        end
    end

    // if(src_col <= dest_col)
    // begin
    //     for(Integer k = 0; k < )
    // end

    // Printing all the value of the routing_mesh 
    for(Integer i = 0; i < len; i = i+1)
    begin
        $display("routing_mesh[%d]: %d", i, routing_mesh[i]);
    end

endrule


// method Action the_assign(int src);
//     source <= src;
// endmethod 

// method the_answer();
//     return mesh;
// endmethod


endmodule: mesh_network
endpackage :  mesh
