/***************************************************************************
*
* Project           		    :  PROJECT-3
* Name of the file	     	    :  hypercube.bsv
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/


package  hypercube;


(* synthesize *)
module hypercube_network();

Integer size = 8;
Int#(32) src = 001;
Int#(32) dest = 110;


Reg#(Int#(32)) source[3];
for(Integer i = 0; i < 3; i = i + 1)
    source[i] <- mkRegU;

Reg#(Int#(32)) destination[size];
for(Integer i = 0; i < 3; i = i + 1)
    destination[i] <- mkRegU;

Reg#(Int#(32)) hypercube[size];
for(Integer i = 0; i < size; i = i + 1)
    hypercube[i] <- mkRegU;

rule load_hypercube_with_value;
    hypercube[0] <= 1000;
    hypercube[1] <= 1001;
    hypercube[2] <= 1010;
    hypercube[3] <= 1011;
    hypercube[4] <= 100;
    hypercube[5] <= 101;
    hypercube[6] <= 110;
    hypercube[7] <= 111;
endrule

rule print_hypercube;
    for(Integer i = 0; i < size; i = i + 1)
        $display("Hypercube: %d", hypercube[i]);
endrule

rule load_source_destination_in_array;
    Int#(32) rem = 0;
    Int#(32) temp_src = 0, temp_dest = 0;
    temp_src = src;
    temp_dest = dest;
    for(Integer i = 2; i >= 0; i = i - 1)
    begin
        rem = temp_src % 10;
        source[i] <= rem;
        temp_src = temp_src / 10;
    end

    rem = 0;
    for(Integer i = 2; i >= 0; i = i - 1)
    begin
        rem = temp_dest % 10;
        destination[i] <= rem;
        temp_dest = temp_dest / 10;
    end
    

    // for(Integer i = 0; i < 3; i = i + 1)
    //     $display("the array source:%d", source[i]);

    // for(Integer i = 0; i < 3; i = i + 1)
    //     $display("the array destination:%d", destination[i]);

endrule


rule hypercube_routing;

    for(Integer i = 0; i < 3; i = i + 1)
        $display("%d", source[i]);

    Integer path = 0;
    for(Integer i = 0; i < 3; i = i + 1)
    begin
        if(source[i] != destination[i])
            path = path + 1;
    end 

    //Storing the destination array in two different array
    Int#(32) temp_array[3];
    Int#(32) temp[3];

    for(Integer i = 0; i < 3; i = i + 1)
    begin
        temp_array[i] = source[i];
        temp[i] = source[i];
    end

    if(path == 2)
    begin
        if(temp_array[2] == 0)
            temp_array[2] = 1;

        else if(temp_array[2] == 1)
            temp_array[2] = 0;
        
        for(Integer i = 0; i < 3; i = i + 1)
            $display("%d", temp_array[i]);
        
        $display("\n");
    end

    else if(path == 3)
    begin
        for(Integer i = 0; i < 3; i = i + 1)
            temp_array[i] = temp[i];
        
        for(Integer i = 1; i < 3; i = i +1)
        begin
            for(Integer j = 0; j < 3; j = j + 1)
                temp_array[j] = temp[j];

            if(i == 1)
            begin
                if(temp_array[i] == 1)
                    temp_array[i] = 0;
                else
                    temp_array[i] = 1;

                if(temp_array[i+1] == 1)
                    temp_array[i+1] = 0;
                else
                    temp_array[i+1] = 1;
            end
            if(i == 2)
            begin
                if(temp_array[i] == 1)
                    temp_array[i] = 0;
                else
                    temp_array[i] = 1;
            end

            for(Integer k = 0; k < 3; k = k + 1)
                $display("%d", temp_array[k]);
            $display("\n");
        end
    end
    for(Integer i = 0; i < 3; i = i + 1)
        $display("%d", destination[i]);
endrule

endmodule: hypercube_network
endpackage : hypercube
