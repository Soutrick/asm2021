/***************************************************************************
*
* Project           		    :  PROJECT-3
* Name of the file	     	    :  chain.bsv
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/


package  chain;

interface Ifc_type;
    method int the_answer();
endinterface: Ifc_type

(* synthesize *)
module chain_network(Ifc_type);

Integer size = 20;                 // Setting Size
Integer src = 3;
Integer dest = 11;
Reg#(int) temp <- mkReg(0);
Reg#(int) arr[size];
Reg#(Bit#(4)) source <- mkReg(0); 
Reg#(Bit#(4)) destination <- mkReg(0);
Reg#(int) count <- mkReg(0);
Reg#(int)routing_chain[size];
    
// Int#(32) check = source;

for(Integer i=0; i < size; i = i+1)
    arr[i] <- mkRegU;

Int#(32) temp_arr[50];
for (Integer i = 0; i < size; i=i+1)
    temp_arr[i] = fromInteger(i * 1);


for (Integer i = 0; i < size; i = i+1)
    rule load_array;
        arr[i] <= temp_arr[i]; 
    endrule

// rule display_array_chain;
//     for (Integer i = 0; i < size; i= i+1)
//         $display("arr1[i] = %d", arr[i]);
// endrule

rule chain_routing(source >=0 && destination <=10);
    $display("Source: %x", source);
    $display("Destination: %x", destination); 
    // for(Bit#(4) j = source; j < destination ; j = j+1)
    //     $display("I: %x", j);
    // count <= count + 1;
    // routing_chain[count] <= arr[i]; 
endrule

rule head_node_even((size % 2) == 0);
    $display("Head_node:%d ", arr[size / 2]);
endrule

rule head_node_odd((size % 2) != 0);
    Integer temp = (size+1)/2;
    $display("Head_node:%d ", arr[temp]);
endrule

for(Integer j = src; j <= dest ; j = j+1)
    rule print_chain_routing;
        $display("%d", arr[j]);
    endrule

method int the_answer();
    // for(Integer j = src; j <= dest; j++)
        return arr[size/2];
endmethod


// rule display_temp_array;
//     for (Integer i = 0; i < size; i= i+1)
//         $display("temp_arr[i] = %d", temp_arr[i]);
// endrule


// method Action the_assign(Bit#(4) src, Bit#(4) dest);
//     source <= src;
//     destination <= dest;
//     // $display("");
// endmethod 

// method the_answer();
//     return temp;
// endmethod


endmodule: chain_network

endpackage :  chain
