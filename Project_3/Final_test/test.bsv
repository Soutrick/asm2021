/***************************************************************************
*
* Project           		    :   
* Name of the file	     	    :   
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/


package test;


// interface Ifc_type1;
//     method Action the_assign(int x);
// endinterface: Ifc_type1


(* synthesize *)
module test_network(Ifc_type);

/*
*   The network variable is a deciding factor 
*   1 for Chain Network
*   2 for Ring Network
*   3 for Mesh Network
*   4 for Folded Torus Network
*   5 for Hypercube
*/
Reg#(int) a <- mkReg(0);

/* In L0 1st checking how many number is there if it contain 
 * three digit then it could be Mess Network or Folded_torus 
 * Network, if it contains two digit then it could be Chain 
 * Network, Ring Network or Hypercube Network.
 * 
 * For Mesh and Folded_Torus first two digit/bit are row and 
 * coloumn consequtively.
 * For chain, ring and hypercube first bit are number of nodes.
 * The last bit in each case are the help to find the which network. 
 * */

Int#(32) l0 = 233;              // Giving the LO network
int network = 0;                
int row = 0, col = 0, total_network = 0;
network = l0%10;                // Deciding factor for network
if(network == 3 && network == 4)
begin
    l0 = l0/10;
    col = l0%10;
    row = l0/10;
    total_network = row * col;
end
else
begin
    l0 = l0/10;
    total_network = l0;
end

/* Here l1 connection which gives us the idea about the different network */
Int#(32) l1 = 23531;

network = 0;
for(int i = 1; i < 6; i = i+1)
begin
    network = l1%10;
    rule delete;
        $display("Network_id:%d", network);
    endrule
    l1 = l1/10;
    // network = 5;
    if(network == 1)
    begin
        Ifc_type chain <- chain_network;
    end
    else if(network == 2)
    begin
        Ifc_type ring <- ring_network;
    end
     else if(network == 3)
    begin
        Ifc_type mesh <- mesh_network;
    end
    else if(network == 4)
    begin
        Ifc_type folded_torus <- folded_torus_network;
    end
    else if(network == 5)
    begin
        Ifc_type hypercube <- hypercube_network;
    end
end

rule display;
    $display("... .. .STARTING... .. .");
endrule

// rule print_network;
//     $display("The value of a: %d", a);
//     $display("The value of a: %x", a);
// endrule

// method Action the_assign(int x);
//     a <= x;
// endmethod

method int the_answer();
    return 22;
endmethod

endmodule: test_network


interface Ifc_type;
    // method Action the_assign(int x);
    method int the_answer();
endinterface: Ifc_type


/*================ CHAIN NETWORK ================*/
(* synthesize *)
module chain_network(Ifc_type);

Integer size = 9;                          // Setting Chian_Network Size as 50
Integer src = 5;
Integer dest = 3;
Reg#(int) temp <- mkReg(0);
Reg#(int) arr[size];
Reg#(Bit#(4)) source <- mkReg(0); 
Reg#(Bit#(4)) destination <- mkReg(0);
Reg#(int) count <- mkReg(0);
Reg#(int)routing_chain[size];
    
// Int#(32) check = source;


for(Integer i=0; i < size; i = i+1)
    arr[i] <- mkRegU;

Int#(32) temp_arr[50];
for (Integer i = 0; i < size; i=i+1)
    temp_arr[i] = fromInteger(i * 1);


for (Integer i = 0; i < size; i = i+1)
    rule load_array;
        arr[i] <= temp_arr[i]; 
    endrule


// rule head_node_even((size % 2) == 0);
//     $display("Head_node:%d ", arr[size / 2]);
// endrule

// rule head_node_odd((size % 2) != 0);
//     Integer temp = (size+1)/2;
//     $display("Head_node:%d ", arr[temp]);
// endrule

if(dest > src)
begin
    for(Integer j = src; j <= dest ; j = j+1)
        rule print_chain_routing;
            $display("%d", arr[j]);
        endrule
end
else
begin
    for(Integer j = src; j >= dest ; j = j-1)
        rule print_chain_routing;
            $display("%d", arr[j]);
        endrule
end

// method int the_answer();
//     // for(Integer j = src; j <= dest; j++)
//         return arr[size/2];
// endmethod

// method int the_answer();
//     return 22;
// endmethod

rule space_print;
    $display(" ");
endrule

endmodule: chain_network


/*================ RING NETWORK ================*/
(* synthesize *)
module ring_network(Ifc_type);

Integer size = 8;                 // Restricting Size of Ring_Network as 50
// Integer step = 0;
Integer src = 4;
Integer dest = 7;
Int#(32) ring[size];
Reg#(int) source <- mkReg(0); 
Reg#(int) destination <- mkReg(0);


Reg#(Int#(32)) routing_ring[size];
for(Integer i=0; i < size; i = i+1)
    routing_ring[i] <- mkRegU;

for(Integer i=0; i < size; i = i+1)
    ring[i] = fromInteger(i * 1);


// rule display_array_chain;
//     for (Integer i = 0; i < size; i= i+1)
//         $display("arr1[i] = %d", arr[i]);
// endrule


// rule head_node_even((size % 2) == 0);
//     $display("Head_node:%d ", ring[size / 2]);
// endrule

// rule head_node_odd((size % 2) != 0);
//     Integer temp = (size+1)/2;
//     $display("Head_node:%d ", ring[temp]);
// endrule

rule ring_routing(src >=0 && dest <= size);
    // $display("Source: %d", source);
    // $display("Destination: %d", destination); 
    Integer count  = 0;
    Integer deciding = size/2;
    Integer temp = 0;

    if(dest > src)
    begin
        if((dest - src) < deciding)                     //Clockwise Movenment
        begin
            for(Integer i = src; i <= dest; i = i+1)
            begin
                routing_ring[count] <= ring[i];
                count = count + 1;
            end
        end
        if ((dest - src) > deciding)                    //Anti-clockwise Movenment
        begin
            for(Integer i = 0; i <= dest-src; i = i+1)
            begin
                if((src - i) > 0)
                begin
                    routing_ring[count] <= ring[src - i];
                    count = count + 1;
                end
                else if((src -1) <= 0)
                begin
                    routing_ring[count] <= ring[size - temp];
                    temp = temp + 1;
                    count = count + 1;
                end
            end
        end
    end
    else if (src > dest)
    begin
        count = 0;
        if((src - dest) < deciding)         // Anti-clockwise Movenment
        begin
            for(Integer i = 0; i <= src-dest; i = i + 1)
            begin
                routing_ring[count] <= ring[src - i];
                count = count + 1;
            end
        end
        if ((src - dest) > deciding)        // Clockwise Movenment
        begin
            for(Integer i = 0; i <= src-dest; i = i + 1)
            begin
                if((src + i) <= size)
                begin
                    routing_ring[count] <= ring[src + i];
                    count = count + 1;
                end
                else
                begin
                    routing_ring[count] <= ring[1+temp];
                    temp = temp + 1;
                    count = count + 1;
                end
            end
        end
    end

    for(Integer j = 0; j < count; j = j+1)
    begin
        // $display("routing_ring[%d]=%d", j, routing_ring[j]);
        $display("%d",routing_ring[j]);
    end
endrule

rule space_print;
    $display(" ");
endrule

endmodule: ring_network


/*=================== MESH NETWORK =======================*/
(* synthesize *)
module mesh_network(Ifc_type);

Integer row = 3;                            // Restricting the row to 3
Integer col = 4;                            // Restricting the col to 4
Int#(32) src = 4;
Int#(32) dest = 9;
// Reg#(int) source <- mkReg(0);
Int#(32) mesh[row][col];

function Bool checking( td i, td j ) provisos( Eq#(td) );
    return ( i == j);
endfunction

function td nop( td i );                    // No Operation function
return i;
endfunction

// Creating a mesh[row][col]
Integer count = 0;
for(Integer i = 0; i < row; i = i+1)
begin
    for(Integer j = 0; j < col; j = j+1)
    begin
        count = count + 1;
        mesh[i][j] = fromInteger(count);
    end
end

// Register a routing_mesh[row * col]
Reg#(Int#(32)) routing_mesh[row * col];
for(Integer j = 0; j < row*col; j = j+1)
    routing_mesh[j] <- mkRegU;


// rule mesh_network_print;
//     for(Integer i = 0; i < row; i = i+1)
//         for(Integer j = 0; j < col; j = j+1)
//             $display("mesh[%d][%d]: %d", i, j, mesh[i][j]);
// endrule


// rule head_node;
//     Integer head_node_row = 0, head_node_col = 0;
//     if(row == col)
//     begin
//         if(row % 2 == 0)
//         begin
//             head_node_row = row/2;
//             head_node_col = col/2;
//         end
//         else
//         begin
//             head_node_row = (row+1)/2;
//             head_node_col = (col+1)/2;
//         end
//     end
//     else
//     begin
//         if(row % 2 == 0)
//             head_node_row = row/2;
//         else
//             head_node_row = (row+1)/2;
//         if(col % 2 == 0)
//             head_node_col = col/2;
//         else
//             head_node_col = (col+1)/2;
//     end

//     $display("Head_Node:%d", mesh[head_node_row][head_node_col]);

// endrule


rule mesh_routing;
    Integer src_row = 0, src_col = 0;
    Integer dest_row = 0, dest_col = 0;
    for(Integer i = 0; i < row; i = i+1)
    begin
        for(Integer j = 0; j < col ; j = j+1)
        begin
            if(src == mesh[i][j])
            begin
                src_row = i;
                src_col = j;
            end
            if(dest == mesh[i][j])
            begin
                dest_row = i;
                dest_col = j;
            end
        end
    end
    // $display("src_row:%d, src_col:%d, dest_row:%d, dest_col:%d", src_row, src_col, dest_row, dest_col);


    //Finding the routing of Mesh
    Integer len = 0, deciding = 0, len_temp = 0;
    if(src_col <= dest_col)                                             // Source should be left side of the destination
    begin
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_mesh[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == mesh[i][src_col + k])                    // Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_mesh[len] <= mesh[i][src_col + k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_mesh[len] <= mesh[src_row][src_col + k];    // X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            else
                $display(" nop is ", nop(10));
        end
    end
    else                                                                // Source should be right side of the destination
    begin  
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_mesh[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == mesh[i][src_col - k])                    // Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_mesh[len] <= mesh[i][src_col - k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_mesh[len] <= mesh[src_row][src_col - k];    //X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            else
                $display(" nop is ", nop(10));
        end
    end

    // if(src_col <= dest_col)
    // begin
    //     for(Integer k = 0; k < )
    // end

    // Printing all the value of the routing_mesh 
    for(Integer i = 0; i < len; i = i+1)
    begin
        $display("%d", routing_mesh[i]);
    end

endrule


// method Action the_assign(int src);
//     source <= src;
// endmethod 

// method the_answer();
//     return mesh;
// endmethod

rule space_print;
    $display(" ");
endrule

endmodule: mesh_network


/*====================== FOLDED TORUS NETWORK ======================*/
(* synthesize *)
module folded_torus_network(Ifc_type);

Integer row = 5;                                        // Restricting row to 5 
Integer col = 4;                                        // Restricting col to 4
Int#(32) src = 4;
Int#(32) dest = 15;
// Reg#(int) source <- mkReg(0);
Int#(32) folded_torus[row][col];


function td nop( td i );
return i;
endfunction

// Creating a folded_torus[row][col]
Integer count = 0;
for(Integer i = 0; i < row; i = i+1)
begin
    for(Integer j = 0; j < col; j = j+1)
    begin
        count = count + 1;
        folded_torus[i][j] = fromInteger(count);
    end
end

// Register a routing_folded_torus[row * col]
Reg#(Int#(32)) routing_folded_torus[row * col];
for(Integer j = 0; j < row*col; j = j+1)
    routing_folded_torus[j] <- mkRegU;


// rule folded_torus_network_print;
//     for(Integer i = 0; i < row; i = i+1)
//         for(Integer j = 0; j < col; j = j+1)
//             $display("folded_torus[%d][%d]: %d", i, j, folded_torus[i][j]);
// endrule


// rule head_node;
//     Integer head_node_row = 0, head_node_col = 0;
//     if(row == col)
//     begin
//         if(row % 2 == 0)
//         begin
//             head_node_row = row/2;
//             head_node_col = col/2;
//         end
//         else
//         begin
//             head_node_row = (row+1)/2;
//             head_node_col = (col+1)/2;
//         end
//     end
//     else
//     begin
//         if(row % 2 == 0)
//             head_node_row = row/2;
//         else
//             head_node_row = (row+1)/2;
//         if(col % 2 == 0)
//             head_node_col = col/2;
//         else
//             head_node_col = (col+1)/2;
//     end

//     $display("Head_Node:%d", folded_torus[head_node_row][head_node_col]);

// endrule


rule folded_torus_routing;
    Integer src_row = 0, src_col = 0;
    Integer dest_row = 0, dest_col = 0;
    for(Integer i = 0; i < row; i = i+1)
    begin
        for(Integer j = 0; j < col ; j = j+1)
        begin
            if(src == folded_torus[i][j])
            begin
                src_row = i;
                src_col = j;
            end
            if(dest == folded_torus[i][j])
            begin
                dest_row = i;
                dest_col = j;
            end
        end
    end
    // $display("src_row:%d, src_col:%d, dest_row:%d, dest_col:%d", src_row, src_col, dest_row, dest_col);


    //Finding the routing of folded_torus
    Integer len = 0, deciding = 0, len_temp = 0;
    if(src_col <= dest_col) /*Source should be left side of the destination*/
    begin
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_folded_torus[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == folded_torus[i][src_col + k])                                //Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_folded_torus[len] <= folded_torus[i][src_col + k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_folded_torus[len] <= folded_torus[src_row][src_col + k];        //X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            // else
            //     $display(" nop is ", nop(10));
        end
    end
    else /*Source should be right side of the destination*/
    begin  
        for(Integer k = 0; k < col; k = k + 1)
        begin
            deciding = 0;
            if(routing_folded_torus[len_temp] != dest)
            begin
                for(Integer i = 0; i < row; i = i + 1)
                begin
                    if(dest == folded_torus[i][src_col - k])                                //Y-Movenment
                        deciding = 1;
                end
                if(deciding == 1)
                begin
                    for(Integer i = src_row; i <= dest_row; i = i + 1)
                    begin
                        routing_folded_torus[len] <= folded_torus[i][src_col - k];
                        len = len + 1;
                    end
                end
                else
                begin
                    routing_folded_torus[len] <= folded_torus[src_row][src_col - k];        //X-Movenment
                    len = len + 1;
                end
                len_temp = len - 1;
            end
            // else
            //     $display(" nop is ", nop(10));
        end
    end

    // if(src_col <= dest_col)
    // begin
    //     for(Integer k = 0; k < )
    // end

    // Printing all the value of the routing_folded_torus 
    for(Integer i = 0; i < len; i = i+1)
    begin
        $display("%d", routing_folded_torus[i]);
    end

endrule


// method Action the_assign(int src);
//     source <= src;
// endmethod 

// method the_answer();
//     return mesh;
// endmethod

rule space_print;
    $display(" ");
endrule

endmodule: folded_torus_network


/*==================== HYPERCUBE NETWORK =========================*/
(* synthesize *)
module hypercube_network(Ifc_type);

Integer size = 8;
Int#(32) src = 000;
Int#(32) dest = 111;


Reg#(Int#(32)) source[3];
for(Integer i = 0; i < 3; i = i + 1)
    source[i] <- mkRegU;

Reg#(Int#(32)) destination[size];
for(Integer i = 0; i < 3; i = i + 1)
    destination[i] <- mkRegU;

Reg#(Int#(32)) hypercube[size];
for(Integer i = 0; i < size; i = i + 1)
    hypercube[i] <- mkRegU;

rule load_hypercube_with_value;
    hypercube[0] <= 1000;
    hypercube[1] <= 1001;
    hypercube[2] <= 1010;
    hypercube[3] <= 1011;
    hypercube[4] <= 100;
    hypercube[5] <= 101;
    hypercube[6] <= 110;
    hypercube[7] <= 111;
endrule

// rule print_hypercube;
//     for(Integer i = 0; i < size; i = i + 1)
//         $display("Hypercube: %d", hypercube[i]);
// endrule

rule load_source_destination_in_array;
    Int#(32) rem = 0;
    Int#(32) temp_src = 0, temp_dest = 0;
    temp_src = src;
    temp_dest = dest;
    for(Integer i = 2; i >= 0; i = i - 1)
    begin
        rem = temp_src % 10;
        source[i] <= rem;
        temp_src = temp_src / 10;
    end

    rem = 0;
    for(Integer i = 2; i >= 0; i = i - 1)
    begin
        rem = temp_dest % 10;
        destination[i] <= rem;
        temp_dest = temp_dest / 10;
    end
    

    // for(Integer i = 0; i < 3; i = i + 1)
    //     $display("the array source:%d", source[i]);

    // for(Integer i = 0; i < 3; i = i + 1)
    //     $display("the array destination:%d", destination[i]);

endrule


rule hypercube_routing;

    for(Integer i = 0; i < 3; i = i + 1)
        $display("%d", source[i]);
    
    $display(" ");

    Integer path = 0;
    for(Integer i = 0; i < 3; i = i + 1)
    begin
        if(source[i] != destination[i])
            path = path + 1;
    end 

    //Storing the destination array in two different array
    Int#(32) temp_array[3];
    Int#(32) temp[3];

    for(Integer i = 0; i < 3; i = i + 1)
    begin
        temp_array[i] = source[i];
        temp[i] = source[i];
    end

    if(path == 2)
    begin
        if(temp_array[2] == 0)
            temp_array[2] = 1;

        else if(temp_array[2] == 1)
            temp_array[2] = 0;
        
        for(Integer i = 0; i < 3; i = i + 1)
            $display("%d", temp_array[i]);
        
        $display("\n");
    end

    else if(path == 3)
    begin
        for(Integer i = 0; i < 3; i = i + 1)
            temp_array[i] = temp[i];
        
        for(Integer i = 1; i < 3; i = i +1)
        begin
            for(Integer j = 0; j < 3; j = j + 1)
                temp_array[j] = temp[j];

            if(i == 1)
            begin
                if(temp_array[i] == 1)
                    temp_array[i] = 0;
                else
                    temp_array[i] = 1;

                if(temp_array[i+1] == 1)
                    temp_array[i+1] = 0;
                else
                    temp_array[i+1] = 1;
            end
            if(i == 2)
            begin
                if(temp_array[i] == 1)
                    temp_array[i] = 0;
                else
                    temp_array[i] = 1;
            end

            for(Integer k = 0; k < 3; k = k + 1)
                $display("%d", temp_array[k]);
            $display("\n");
        end
    end
    for(Integer i = 0; i < 3; i = i + 1)
        $display("%d", destination[i]);
endrule

rule space_print;
    $display(" ");
endrule

endmodule: hypercube_network

endpackage: test