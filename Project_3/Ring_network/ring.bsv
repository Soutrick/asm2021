/***************************************************************************
*
* Project           		    :  PROJECT-3
* Name of the file	     	    :  ring.bsv
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/


package  ring;

interface Ifc_type1;
    method Action the_assign(int src, int dest);
    // method int the_answer();
endinterface

(* synthesize *)
module ring_network(Ifc_type1);

Integer size = 20;                 // Setting Size
// Integer step = 0;
Integer src = 18;
Integer dest = 15;
Int#(32) ring[size];
Reg#(int) source <- mkReg(0); 
Reg#(int) destination <- mkReg(0);


Reg#(Int#(32)) routing_ring[size];
for(Integer i=0; i < size; i = i+1)
    routing_ring[i] <- mkRegU;

for(Integer i=0; i < size; i = i+1)
    ring[i] = fromInteger(i * 1);


// rule display_array_chain;
//     for (Integer i = 0; i < size; i= i+1)
//         $display("arr1[i] = %d", arr[i]);
// endrule
rule head_node_even((size % 2) == 0);
    $display("Head_node:%d ", ring[size / 2]);
endrule

rule head_node_odd((size % 2) != 0);
    Integer temp = (size+1)/2;
    $display("Head_node:%d ", ring[temp]);
endrule

rule ring_routing(src >=0 && dest <= size);
    // $display("Source: %d", source);
    // $display("Destination: %d", destination); 
    Integer count  = 0;
    Integer deciding = size/2;
    Integer temp = 0;

    if(dest > src)
    begin
        if((dest - src) < deciding)    //Clockwise Movenment
        begin
            for(Integer i = src; i <= dest; i = i+1)
            begin
                routing_ring[count] <= ring[i];
                count = count + 1;
            end
        end
        if ((dest - src) > deciding)   //Anti-clockwise Movenment
        begin
            for(Integer i = 0; i <= dest-src; i = i+1)
            begin
                if((src - i) > 0)
                begin
                    routing_ring[count] <= ring[src - i];
                    count = count + 1;
                end
                else if((src -1) <= 0)
                begin
                    routing_ring[count] <= ring[size - temp];
                    temp = temp + 1;
                    count = count + 1;
                end
            end
        end
    end
    else if (src > dest)
    begin
        count = 0;
        if((src - dest) < deciding)         // Anti-clockwise Movenment
        begin
            for(Integer i = 0; i <= src-dest; i = i + 1)
            begin
                routing_ring[count] <= ring[src - i];
                count = count + 1;
            end
        end
        if ((src - dest) > deciding)        // Clockwise Movenment
        begin
            for(Integer i = 0; i <= src-dest; i = i + 1)
            begin
                if((src + i) <= size)
                begin
                    routing_ring[count] <= ring[src + i];
                    count = count + 1;
                end
                else
                begin
                    routing_ring[count] <= ring[1+temp];
                    temp = temp + 1;
                    count = count + 1;
                end
            end
        end
    end

    // for(Int#(32) j = src; j <= dest ; j = j+1)
    // begin
    //     routing_ring[count] <= j;
    //     count = count + 1;
    // end
    // step <= count;
    // $display("Step: %d", step);
    for(Integer j = 0; j < count; j = j+1)
        $display("routing_ring[%d]=%d", j, routing_ring[j]);
endrule


// rule print_ring_routing;
//     $display("Step: %d", step);
//     // for(Integer j = 0; j < test; j = j+1)
//     //     $display("%d,  %d", path[j], j);
// endrule



method Action the_assign(int src, int dest);
    source <= src;
    destination <= dest;
    // $display("");
endmethod 

// method the_answer();
//     return temp;
// endmethod


endmodule: ring_network
endpackage : ring
