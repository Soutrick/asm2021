# Simple tests for an adder module
import cocotb
import random
import os

from cocotb.clock import Clock
from cocotb.decorators import coroutine
from cocotb.triggers import Timer, RisingEdge, ReadOnly, FallingEdge
from cocotb_bus.monitors import Monitor
from cocotb_bus.drivers import BitDriver
from cocotb.binary import BinaryValue
from cocotb.regression import TestFactory
from cocotb_bus.scoreboard import Scoreboard
from cocotb.result import TestFailure, TestSuccess


#================================================================================================

# A = int(os.environ['COUNT'])
# src = int(input("Enter the source: "))
# dest = int(input("Enter the dest: "))
# print("source:", src , "and dest:", dest)
@cocotb.test()
def folded_torus_network(dut):  #14
    cocotb.fork(Clock(dut.CLK, 10,).start())
    clkedge = RisingEdge(dut.CLK)
    dut.RST_N = 0
    # dut.EN_the_assign = 1
 

    # Modification
    yield Timer(5,units='ps')
    dut.RST_N <= 1
    # dut.EN_the_assign = 1
    
    # dut.the_assign_src <= BinaryValue(value=src,n_bits=32,bigEndian=False)
    # dut.the_assign_dest <= BinaryValue(value=dest,n_bits=32,bigEndian=False)

    yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge
    # yield clkedge

    # dutResultBin = dut.the_answer.value
    # print(f'Result: dut_result={str(dutResultBin.value)}')