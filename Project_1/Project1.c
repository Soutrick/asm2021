/***************************************************************************
* Project           		    :  PROJECT-1
* Name of the file	     	    :  project1.c
* Brief Description of file     :  It helps to find all the combination of the edge
* Name of Author    	        :  Soutrick Roy Chowdhury,Dasari Monisha
* Email ID                      :  soutrickofficial@gmail.com,cs18b011@smail.iitm.ac.in
***************************************************************************/

/*gcc Project1.c -o project1 -lm*/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

FILE *out_file;
FILE *L0Topology;
FILE *L1Topology;


/** @fn int node_value_m(int len, char *array)
 * @brief Function helps to find the value of no. of coloumn 
 * @param len Passing the length of the string reading from txt file
 * @param array Passing the array
 */
int node_value_m(int len, char *array)
{
    int count = 0, start = 0;
    for (int k = 3; k < len-1; k++)
    {
        // printf("\n%c", array[k]);
        if (array[k] == ',')
        {
            start = k;
            break;
        }
    }
    for (int k = start+1; k < len-1 ; k++)
    {
        if (array[k] == ')')
        {
            break;
        }
        else
        {
            count = count * 10;
            count = count + (array[k] - '0');
        }
    }
    return count;
}


/** @fn int node_value(int len, char *array)
 * @brief Function helps to find the value of no. of row 
 * @param len Passing the length of the string reading from txt file
 * @param array Passing the array
 */
int node_value(int len, char *array)
{
    int count = 0;
    for (int k = 3; k < len-1; k++)
    {
        // printf("\n%c", array[k]);
        if (array[k] == ')' || array[k] == ',')
        {
            break;
        }
        else
        {
            count = count * 10;
            count = count + (array[k] - '0');
        }
    }
    return count;
}


/** @fn void node(int n,int k)
 * @brief Doing the intermediate connection for the butterfly
 * @param n Passing the value of n
 * @param k Passing the value of k
 */
void node(int n,int k)
{
	if(n==0) 
	{
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{	
			int t = 2*ii+1;
			printf("L%d: X0_%d\n L%d: X0_%d\n",t-1,ii,t,ii);
            fprintf(out_file,"Node ID: L%d\nLinks: 1\nThe Node connected : X0_%d\nNode ID: L%d\nLinks: 1\nThe Node connected: X0_%d\n",t-1,ii,t,ii);
		}
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{
			int temp = (ii & (1<<(k-2)))>>(k-2),p=pow(2,(k-2)),t=2*ii+1;
			if(temp==0) 
            {
                printf("X0_%d: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii+p);
                fprintf(out_file, "Node Id: X0_%d\nLinks: 4\nThe Node connected: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii+p);
            }
			else 
            {
                printf("X0_%d: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii-p);
                fprintf(out_file, "Node ID:X0_%d\nLinks: 4\nThe Node connected: L%d,L%d,X1_%d,X1_%d\n",ii,t-1,t,ii,ii-p);
            }
		}	 
	}
	else 
	{
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{	
			int t = 2*ii+1;
			printf("R%d: X%d_%d\n R%d: X%d_%d\n",t-1,k-1,ii,t,k-1,ii);
            fprintf(out_file, "Node ID: R%d\nLinks: 1\nThe Node connected: X%d_%d\nNode ID: R%d\nLinks: 1\nThe Node connected: X%d_%d\n",t-1,k-1,ii,t,k-1,ii);
		}
		for(int ii=0;ii<pow(2,(k-1));ii++)
		{
			int t = 2*ii+1;
			if(ii%2==0) 
            {
                printf("X%d_%d: R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii+1);
                fprintf(out_file, "Node ID: X%d_%d\nLinks: 4\nThe Nodes connected R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii+1);
            }
			else 
                printf("X%d_%d: R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii-1);
                fprintf(out_file, "Node ID: X%d_%d\nLinks: 4\nThe Nodes connected R%d,R%d,X%d_%d,X%d_%d\n",k-1,ii,t-1,t,k-2,ii,k-2,ii-1);
		}	 
		
	}
	return;
}

/** @fn int butterfly_network(int k, int id_no)
 * @brief Finally doing the operation for the butterfly network
 * @param k Passing the value of k  
 * @param id_no Passing the value for the id_no for the identification
 */
void butterfly_network(int k, int id_no)
{
	int t=pow(2,(k-1));
	for(int ii=0;ii<k;ii++)
	{
	 	if(ii==0||ii==(k-1)) 
	 	{
	 		node(ii,k);
	 		continue;
	 	}
 		for(int jj=0;jj<t;jj++)
 		{
 			int temp ,jj_next;
 			temp = ((jj & (1 << (k -2 -ii ))) >> (k - 2 - ii));
 			if(temp==0) jj_next = jj + pow(2,(k-ii-2));
 			else jj_next = jj - pow(2,(k-ii-2));
 			printf("X%d_%d: X%d_%d,X%d_%d\n",ii,jj,ii+1,jj,ii+1,jj_next);
            fprintf(out_file, "X%d_%d: X%d_%d,X%d_%d\n",ii,jj,ii+1,jj,ii+1,jj_next);
 		}
	}
	return ;//head node for now R0_0 can be changed a/c if the nodes are considered on both sides or not
}


/** @fn int hypercube_network(int n, int id_no)
 * @brief As it restrict that the number of nodes for the hypercube are 8
 *        so, printing all the combinations of the hypercube.
 * @param n passing the number of nodes for hypercube is 8
 * @param id_no It passes the unique identity number to the Hypercube Tolpology
 * @param head_combination Helps to find the head combination
 * @param start Used to start value for the head combination
 */
int hypercube_network(int n, int id_no, int * head_combination, int start)
{
    int head_node = 0;
    if (start == 1)
    {
        printf("\nNode %d connected to %d %d %d", head_combination[1], head_combination[2], head_combination[5], head_combination[4]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[1], head_combination[2], head_combination[5], head_combination[4]);
        printf("\nNode %d connected to %d %d %d", head_combination[2], head_combination[1], head_combination[6], head_combination[3]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[2], head_combination[1], head_combination[6], head_combination[3]);
        printf("\nNode %d connected to %d %d %d", head_combination[3], head_combination[2], head_combination[4], head_combination[7]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[3], head_combination[2], head_combination[4], head_combination[7]);
        printf("\nNode %d connected to %d %d %d", head_combination[4], head_combination[1], head_combination[3], head_combination[8]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[4], head_combination[1], head_combination[3], head_combination[8]);
        printf("\nNode %d connected to %d %d %d", head_combination[5], head_combination[1], head_combination[6], head_combination[8]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[5], head_combination[1], head_combination[6], head_combination[8]);
        printf("\nNode %d connected to %d %d %d", head_combination[6], head_combination[2], head_combination[5], head_combination[7]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[6], head_combination[2], head_combination[5], head_combination[7]);
        printf("\nNode %d connected to %d %d %d", head_combination[7], head_combination[6], head_combination[3], head_combination[8]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[7], head_combination[6], head_combination[3], head_combination[8]);
        printf("\nNode %d connected to %d %d %d", head_combination[8], head_combination[5], head_combination[7], head_combination[4]);
        fprintf(out_file, "\nNode %d connected to %d %d %d", head_combination[8], head_combination[5], head_combination[7], head_combination[4]);
        printf("\n");
        fprintf(out_file, "\n");
    }
    else
    {
        fprintf(out_file, "\nNode ID: %d_1\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected  2,5,4");
	    fprintf(out_file, "\nNode ID: %d_2\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 1,6,3");
	    fprintf(out_file, "\nNode ID: %d_3\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 2,4,7");
	    fprintf(out_file, "\nNode ID: %d_4\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 1,3,8");
	    // printf("Node 5 connected to 1, 6, 8");
	    fprintf(out_file, "\nNode ID: %d_6\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 2,5,7");
	    fprintf(out_file, "\nNode ID: %d_7\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 6,3,8");
	    fprintf(out_file, "\nNode ID: %d_8\nLinks: 3", id_no);
	    fprintf(out_file, "\nThe Node connected to 5,7,4");

        head_node = 5;  //Considering head_node as 5
        printf("Headnode: %d", head_node);

        return head_node;
    }
}


/** @fn int folded_torus_network(int n, int m, int id_no)
 * @brief Function used to create the folded torus topology , find and returns the head node 
 *        and write all the possible combinations of the node connections to the output.txt.
 * @param n It passes to store the number of row  
 * @param m It passes to store the number of coloumn
 * @param id_no It passes the unique identity number to the Folded Torus Tolpology
 * @param head_combination Helps to find the head combination
 * @param start Used to start value for the head combination
 */
int folded_torus_network(int n, int m, int id_no, int * head_combination, int start)
{
    if (start == 1)
    {
        int folded_torus[n][m];
        int count = 0;

        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                count += 1;
                folded_torus[i][j] = head_combination[count];
            }    
        }
        printf("\n");
        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                printf("%d\t", folded_torus[i][j]);  
            }
            printf("\n");
        }

        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {
            if (i > 1 & i < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
            }
            else if (i == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                for (int j = 3; j <= m; j=j+2)
                {
                    printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[i][j]);
                    fprintf(out_file, ", %d", folded_torus[i][j]);
                }  
                fprintf(out_file, "\n");
            }
            else
            {
                printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                for (int j = m-2; j >= 1; j=j-2)
                {
                    printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[1][j]);
                    fprintf(out_file, ", %d", folded_torus[1][j]);
                }
                printf("\n");
                fprintf(out_file, "\n");
            }    
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {

                if (j > 1 & j < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                }
                else if (j == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                    for (int k = 3; k <= m; k=k+2)
                    {
                        printf("\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i][k]);
                        fprintf(out_file, ", %d", folded_torus[i][k]);
                    }  
                    printf("\n");
                    fprintf(out_file, "\n");
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                    for (int k = m-2; k >= 1; k=k-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[i][j], folded_torus[i][k]);
                        fprintf(out_file, ", %d", folded_torus[i][k]);
                    }
                    printf("\n");
                    fprintf(out_file, "\n");
                }
            
            }     
        }

        count = 0;
        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {

            if (i > 1 & i < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
            }
            else if (i == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                for (int j = 3; j <= m; j=j+2)
                {
                    printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                    fprintf(out_file, ", %d", folded_torus[n][j]);
                }  
                printf("\n");
                fprintf(out_file, "\n");
            }
            else
            {
                count = 0;
                printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                for (int j = m-2; j >= 1; j=j-2)
                {
                    printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                    fprintf(out_file, ", %d", folded_torus[n][j]);
                }
                printf("\n");
                fprintf(out_file, "\n");
            }
        
        } 
    }
    else
    {
        // printf("%d, %d", n, m);
        int folded_torus[n][m];
        int count = 0, head_node_n = 0, head_node_m = 0;

        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                count += 1;
                folded_torus[i][j] = count;
            }    
        }
        printf("\n");
        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                printf("%d\t", folded_torus[i][j]);  
            }
            printf("\n");
        }
        
        if (n == m)
        {
            if (n%2 == 0)
            {
                head_node_n = n/2;
                head_node_m = m/2;
            }
            else
            {
                head_node_n = (n+1)/2;
                head_node_m = (m+1)/2;
            }
        }
        else
        {
            if (n%2 == 0)
            {
                head_node_n = n/2;
            }
            else
            {
                head_node_n = (n+1)/2;
            }

            if (m%2 == 0)
            {
                head_node_m = m/2;
            }
            else
            {
                head_node_m = (m+1)/2;
            }
        }
        printf("The Head Node: %d\n", folded_torus[head_node_n][head_node_m]);


        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[i][j]);
                        fprintf(out_file, ", %d", folded_torus[i][j]);
                    }  
                    fprintf(out_file, "\n");
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[1][j]);
                        fprintf(out_file, ", %d", folded_torus[1][j]);
                    }
                    printf("\n");
                    fprintf(out_file, "\n");
                }
            // }
            
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                // if (i == head_node_n && j == head_node_m)
                // {
                //     continue;
                // }
                // else
                // {
                    if (j > 1 & j < m)
                    {
                        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                    }
                    else if (j == 1)
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = 3; k <= m; k=k+2)
                        {
                            printf("\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i][k]);
                            fprintf(out_file, ", %d", folded_torus[i][k]);
                        }  
                        printf("\n");
                        fprintf(out_file, "\n");
                    }
                    else
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = m-2; k >= 1; k=k-2)
                        {
                            printf("\nThe node %d connected to the %d",folded_torus[i][j], folded_torus[i][k]);
                            fprintf(out_file, ", %d", folded_torus[i][k]);
                        }
                        printf("\n");
                        fprintf(out_file, "\n");
                    }
                // }
            }     
        }

        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        fprintf(out_file, ", %d", folded_torus[n][j]);
                    }  
                    printf("\n");
                    fprintf(out_file, "\n");
                }
                else
                {
                    count = 0;
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        fprintf(out_file, ", %d", folded_torus[n][j]);
                    }
                    printf("\n");
                    fprintf(out_file, "\n");
                }
            // }
        } 
        return folded_torus[head_node_n][head_node_m];

    }
}


/** @fn int mesh_network(int n, int m, int id_no)
 * @brief Function used to create the mesh topology , find and returns the head node 
 *        and write all the possible combinations of the node connections to the output.txt.
 * @param n It passes to store the number of row  
 * @param m It passes to store the number of coloumn
 * @param id_no It passes the unique identity number to the Mesh Tolpology
 * @param head_combination Helps to find the head combination
 * @param start Used to start value for the head combination
 */
int mesh_network(int n, int m, int id_no, int * head_combination, int start)
{
    if (start == 1)
    {
        int mesh[n][m];
        int count = 0;
        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {     
                count += 1;
                mesh[i][j] = head_combination[count];
            }    
        }
        printf("\n");
        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                printf("%d\t", mesh[i][j]);  
            }
            printf("\n");
        }

        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {

            if (i > 1 & i < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
            }
            else if (i == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
            }
            else
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][m], mesh[1][m-1], mesh[1][m], mesh[1+1][m]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
            }
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                if (j > 1 & j < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 4", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j], mesh[i][j+1], mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j]);
                    fprintf(out_file, "The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
                }
                else if (j == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j+1]);
                    fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j-1]);
                    fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
                }           
            }     
        }
        
        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {
            if (i > 1 & i < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
            }
            else if (i == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
            }
            else
            {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][m], mesh[m][n-1], mesh[n][m], mesh[n-1][m]);
                fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
            }
        }
    }
    else
    {
        int mesh[n][m];
        int count = 0, head_node_n = 0, head_node_m = 0;

        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {     
                count += 1;
                mesh[i][j] = count;
            }    
        }
        printf("\n");
        for (int i = 1; i <= n; i++)    
        {    
            for (int j = 1; j <= m; j++)    
            {    
                printf("%d\t", mesh[i][j]);  
            }
            printf("\n");
        }

        if (n == m)
        {
            if (n%2 == 0)
            {
                head_node_n = n/2;
                head_node_m = m/2;
            }
            else
            {
                head_node_n = (n+1)/2;
                head_node_m = (m+1)/2;
            }
        }
        else
        {
            if (n%2 == 0)
            {
                head_node_n = n/2;
            }
            else
            {
                head_node_n = (n+1)/2;
            }

            if (m%2 == 0)
            {
                head_node_m = m/2;
            }
            else
            {
                head_node_m = (m+1)/2;
            }
        }
        printf("The Head Node: %d\n", mesh[head_node_n][head_node_m]);

        // Try to find the combination of the node connected with the other node
        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {
            // if(i == head_node_m)
            // {
            //     continue;
            // }
            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][m], mesh[1][m-1], mesh[1][m], mesh[1+1][m]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
                }
            // }
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                // if (i == head_node_n && j == head_node_m)
                //     continue;

                // else
                // {
                    if (j > 1 & j < m)
                    {
                        printf("\nNode ID: %d_%d\nLinks: 4", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 4", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j], mesh[i][j+1], mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j]);
                        fprintf(out_file, "The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
                    }
                    else if (j == 1)
                    {
                        printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j+1]);
                        fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
                    }
                    else
                    {
                        printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j-1]);
                        fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
                    }           
                // }
            }     
        }
        
        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            //     continue;

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][m], mesh[m][n-1], mesh[n][m], mesh[n-1][m]);
                    fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
                }
            // }
        }  
        return mesh[head_node_n][head_node_m];  
    }
    
}


/** @fn int ring_network(int n, int id_no)
 * @brief Function used to create the ring topology , find and returns the head node 
 *        and write all the possible combinations of the node connections to the output.txt.
 * @param n It passes the length of the Ring Topology
 * @param id_no It passes the unique identity number to the Ring Tolpology
 */
int ring_network(int n, int id_no, int * head_combination, int start)
{
    if(start == 1)
    {
        int i = 1;
        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node: %d Connected to: %d\nThe Node: %d Connected to: %d", head_combination[i], head_combination[i+1], head_combination[i], head_combination[n]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", head_combination[i], head_combination[i+1], head_combination[n]);
        for (int i = 2; i < n; i++)
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe Node: %d Connected to %d\nThe Node: %d Connected to %d", head_combination[i], head_combination[i-1], head_combination[i], head_combination[i+1]);
            fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", head_combination[i], head_combination[i-1], head_combination[i+1]);

        }
        i = n;
        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node %d Connected to: %d\nThe Node %d Connected to: %d\n", head_combination[i], head_combination[i-1], head_combination[i], head_combination[0]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d\n", head_combination[i], head_combination[i-1], head_combination[0]);
    }
    else
    {
        int ring[n];
        int head_node = 0;
        for (int i = 1; i <= n; i++)
        {
            ring[i] = i;
        }

        for (int i = 1; i <= n; i++)
        {
            printf("%d\t", ring[i]);
        }

        srand(time(0));
        head_node = (rand() % (n + 1)) + 0;

        printf("\nHead Node: %d", ring[head_node]);
        
        // Try to find the combination of the node connected with the other node
        int i = 1;
        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node: %d Connected to: %d\nThe Node: %d Connected to: %d", ring[i], ring[i+1], ring[i], ring[n]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i+1], ring[n]);
        for (int i = 2; i < n; i++)
        {
            // if(i == head_node)
            //     continue;
            // else
            // {
                printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                printf("\nThe Node: %d Connected to %d\nThe Node: %d Connected to %d", ring[i], ring[i-1], ring[i], ring[i+1]);
                fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i-1], ring[i+1]);
            // }
        }
        i = n;
        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
        fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node %d Connected to: %d\nThe Node %d Connected to: %d\n", ring[i], ring[i-1], ring[i], ring[0]);
        fprintf(out_file, "\nThe Node: %d Connected to: %d, %d\n", ring[i], ring[i-1], ring[0]);

        return ring[head_node];

    }
}

/** @fn int chain_network(int n, int id_no)
 * @brief Function used to create the chain topology , find and returns the head node 
 *        and write all the possible combinations of the node connections to the output.txt.
 * @param n It passes the length of the Chain Tolopogy
 * @param id_no It passes the unique identity number to the Chain Tolpology
 */
int chain_network(int n, int id_no, int * head_combination, int start)
{

    if (start == 1)
    {
        printf("HEAD COMBINATION\n");
        for (int i = 1; i <= n; i++)
        {
            printf("Head: %d\n", head_combination[i]);
        }

        int i = 1;
        printf("\nThe Node %d Connected to %d", head_combination[i], head_combination[i+1]);
        fprintf(out_file, "\nThe Node %d Connected to %d", head_combination[i], head_combination[i+1]);
        for (int i = 2; i < n; i++)
        {
            printf("\nThe Node %d Connected to %d\nThe Node %d Connected to %d", head_combination[i], head_combination[i-1], head_combination[i], head_combination[i+1]);
            fprintf(out_file, "\nThe Node %d Connected to %d, %d", head_combination[i], head_combination[i-1], head_combination[i+1]);
        }
        i = n;
        printf("\nThe Node %d Connected to %d\n", head_combination[i], head_combination[i-1]);
        fprintf(out_file, "\nThe Node %d Connected to %d\n", head_combination[i], head_combination[i-1]);
    }
    else
    {

        int chain[n];
        int head_node = 0;
        for (int i = 1; i <= n; i++)
        {
            chain[i] = i;
        }

        for (int i = 1; i <= n; i++)
        {
            printf("%d\t", chain[i]);
        }
        if (n%2 == 0)
        {
            head_node = n/2;
        }
        else
        {
            head_node = (n+1)/2;
        }
        printf("\nHead Node: %d", chain[head_node]);

        // Try to find the combination of the node connected with the other node
        int i = 1;
        printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
        fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
        printf("\nThe Node %d Connected to %d", chain[i], chain[i+1]);
        fprintf(out_file, "\nThe Node %d Connected to %d", chain[i], chain[i+1]);
        for (int i = 2; i < n; i++)
        {
            printf("\n\nNode ID: %d_%d\nLinks: 2", id_no, i);
            fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe Node %d Connected to %d\nThe Node %d Connected to %d", chain[i], chain[i-1], chain[i], chain[i+1]);
            fprintf(out_file, "\nThe Node %d Connected to %d, %d", chain[i], chain[i-1], chain[i+1]);
            
        }
        i = n;
        printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
        fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
        printf("\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
        fprintf(out_file, "\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
        return chain[head_node];
    }
}


/** @fn int main()
 * @brief the main function use to read the requied information from the L0Topology.txt
 *        and L1Topology.txt and uses different character to distinguish between topology
 *        such as,
 *        C for Chain Topology
 *        R for Ring Topology
 *        M for Mesh Topology
 *        F for Folded Torus Topology
 *        H for Hypercube Topology
 *        B for Butterfly Tolpology
 *        Then finally writing all the possible combinations to output.txt
 */
void main()
{
    out_file = fopen("output.txt", "w+");
    L0Topology = fopen("L0Topology.txt", "r");

    char read_L0[100];
    char read_L1[200], ch;
    int n = 0 , m = 0, headnode = 0;

    fgets(read_L0, sizeof(read_L0), L0Topology);
    fclose(L0Topology);
    int no_L1_topology = 0;
    if (read_L0[1] == 'C' || read_L0[1] == 'R'|| read_L0[1] == 'B' || read_L0[1] == 'H')
    {
        no_L1_topology = node_value(strlen(read_L0), read_L0);
    }
    else
    {
        n = node_value(strlen(read_L0), read_L0);
        m = node_value_m(strlen(read_L0), read_L0);
        printf("n: %d, m: %d", n, m);
        no_L1_topology = n * m;
    }

    printf("\nNo_L1_topology: %d", no_L1_topology);    
    int Head_node[no_L1_topology+1];

    L1Topology = fopen("L1Topology.txt", "r");

    printf("\nC for Chain\nR for Ring\nM for Mesh\nF for Folded Torus\nH for Hypercube\nB for Butterfly\n");
    for (int i = 1; i <= no_L1_topology; i++)
    {
        fgets(read_L1, sizeof(read_L1), L0Topology);
        printf("%s", read_L1);
        
        printf("\nPlz enter the L1 Network");
        ch = read_L1[1];
#if 1
        switch (ch)
        {
            case 'C':
                printf("\nChain\n");
                n = node_value(strlen(read_L1), read_L1);
                printf("Id number: %d\n", i);
                // fprintf(out_file, "\nChain Network: Id Number: %d", i);
                headnode = chain_network(n, i, 0, 0);
                Head_node[i] = headnode;
            break;
    
            case 'R':
                printf("\nRing\n");
                n = node_value(strlen(read_L1), read_L1);
                printf("Id number: %d\n", i);
                // fprintf(out_file, "\nRing Network: Id Number: %d", i);
                headnode = ring_network(n, i, 0, 0);
                Head_node[i] = headnode;
            break;        
    
            case 'M':
                printf("\nMesh\n");
                n = node_value(strlen(read_L1), read_L1);
                m = node_value_m(strlen(read_L1), read_L1);
                printf("Id number: %d\n", i);
                // fprintf(out_file, "\nMesh Network: Id Number: %d\n", i);
                headnode = mesh_network(n, m, i, 0, 0);
                Head_node[i] = headnode;
            break; 
    
            case 'F':
                printf("\nFolded Torus\n");
                n = node_value(strlen(read_L1), read_L1);
                m = node_value_m(strlen(read_L1), read_L1);
                printf("Id number: %d\n", i);
                // fprintf(out_file, "\nFolded Torus Network: Id Number: %d", i);
                headnode = folded_torus_network(n, m, i, 0, 0);
                Head_node[i] = headnode;
            break;     
    
            case 'H':
                printf("\nHypercube\n");
                printf("The size defined to the Hypercube is 8\n");
                printf("Id number: %d\n", i);
                // fprintf(out_file, "\nHypercube Network: Id Number: %d", i);
                headnode = hypercube_network(8, i, 0, 0);
                Head_node[i] = headnode;
            break; 
    
            case 'B':
                printf("\nButterfly\n");
                printf("\nPlz Enter k if the configuration is 2^k*2^k\n");
                n = node_value(strlen(read_L1), read_L1);
                // fprintf(out_file, "\nButterfly Network: Id Number: %d", i);
                printf("Id number: %d\n", i);
                butterfly_network(n, i);
            break;
    
            default:
                break;          
        }
#endif    
    }
#if 1
    fprintf(out_file, "\nPrinting all the Head Node combination\n");
    char c;
    c = read_L0[1];
    switch (c)
    {
    case 'C':
        n = node_value(strlen(read_L0), read_L0);
        // printf("%d", n);
        chain_network(n, 0, Head_node, 1);
        break;

    case 'R':
        n = node_value(strlen(read_L0), read_L0);
        ring_network(n,  0, Head_node, 1);
    break;        

    case 'M':
        printf("\nMesh\n");
        n = node_value(strlen(read_L0), read_L0);
        m = node_value_m(strlen(read_L0), read_L0);
        mesh_network(n, m, 0, Head_node, 1);
    break; 

    case 'F':
        printf("\nFolded Torus\n");
        n = node_value(strlen(read_L0), read_L0);
        m = node_value_m(strlen(read_L0), read_L0);
        folded_torus_network(n, m,  0, Head_node, 1);
    break;     

    case 'H':
        printf("\nHypercube\n");
        hypercube_network(8, 0, Head_node, 1);
    break; 

    // case 'B':
    //     printf("\nButterfly\n");
    //     printf("\nPlz Enter k if the configuration is 2^k*2^k\n");
    //     n = node_value(strlen(read_L1), read_L1);
    //     fprintf(out_file, "\nButterfly Network: Id Number: %d", i);
    //     printf("Id number: %d\n", i);
    //             butterfly_network(n, i);
    // break;
    
    default:
    break;          
    
    }

    fclose(out_file);
    fclose(L1Topology);
#endif   
} 
