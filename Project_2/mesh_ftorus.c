/***************************************************************************
*
* Project           		    :  PROJECT-2
* Name of the file	     	    :  project2.c
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

int path[50], len = 0;

/** @fn int check_in_col(int n, int m, int mesh_torus[n][m], int dest, int col)
 * @brief 
 * @param int n
 * @param int m
 * @param int mesh_torus[n][m]
 * @param int col
 */
int check_in_col(int n, int m, int mesh_torus[n][m], int dest, int col)
{
    printf("\n In check_in_col");
    for (int i = 1; i <= n; i++)
    {
        if (dest == mesh_torus[i][col])
        {
            printf("\n Y-movenment");
            return 1;
        }
    }
}

/** @fn int routing_folded_torus(int n, int m, int folded_torus[n][m])
 * @brief 
 * @param int n
 * @param int m
 * @param int folded_torus[n][m]
 */
int routing_folded_torus(int n, int m, int folded_torus[n][m])
{
    printf("\n The array\n");

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("%d\t", folded_torus[i][j]);  
        }
        printf("\n");
    }

    int src, dest;
    int src_row = 0, src_col = 0, dest_row = 0, dest_col = 0;
    printf("\n Inside the routing folded_torus\n");

    printf("Enter the source and Destination");
    scanf("%d %d", &src, &dest);

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            // printf("%d\t", mesh[i][j]);  
            // Finding the position of the source and destination
            if (src == folded_torus[i][j])
            {
                printf("\n The src location:[%d] [%d]", i, j);
                src_row = i;
                src_col = j;
            }
            if (dest == folded_torus[i][j])
            {
                printf("\n The dest location:[%d] [%d]", i, j);
                dest_row = i;
                dest_col = j;
            }
        }
    }

    if(src > 0 && dest <= folded_torus[n][m])
    {
        if(src_col <= dest_col) /*SOURCE should be on Left side than DESTINATION*/
        {
            printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, folded_torus, dest, src_col+k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = folded_torus[i][src_col+k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        printf("\n X-movenment");
                        path[len] = folded_torus[src_row][src_col+k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }
        else  /*SOURCE should be on right side than DESTINATION*/
        {
            printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, folded_torus, dest, src_col-k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = folded_torus[i][src_col-k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        printf("\n X-movenment");
                        path[len] = folded_torus[src_row][src_col-k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }   
    }
    else
    {
        printf("\nPlease give source and destination in the range");
    }
    return 0;
}

/** @fn int folded_torus_network(int n, int m, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n 
 * @param int m
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int folded_torus_network(int n, int m, int id_no, int * head_combination, int start)
{
    int folded_torus[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            count += 1;
            folded_torus[i][j] = count;
        }    
    }
    printf("\n");
    // for (int i = 1; i <= n; i++)    
    // {    
    //     for (int j = 1; j <= m; j++)    
    //     {    
    //         printf("%d\t", folded_torus[i][j]);  
    //     }
    //     printf("\n");
    // }
    
    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }
        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", folded_torus[head_node_n][head_node_m]);
    
#if 0

        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[i][j]);
                        // fprintf(out_file, ", %d", folded_torus[i][j]);
                    }  
                    // fprintf(out_file, "\n");
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[1][j]);
                        // fprintf(out_file, ", %d", folded_torus[1][j]);
                    }
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
            // }
            
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                // if (i == head_node_n && j == head_node_m)
                // {
                //     continue;
                // }
                // else
                // {
                    if (j > 1 & j < m)
                    {
                        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                    }
                    else if (j == 1)
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = 3; k <= m; k=k+2)
                        {
                            printf("\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i][k]);
                            // fprintf(out_file, ", %d", folded_torus[i][k]);
                        }  
                        printf("\n");
                        // fprintf(out_file, "\n");
                    }
                    else
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = m-2; k >= 1; k=k-2)
                        {
                            printf("\nThe node %d connected to the %d",folded_torus[i][j], folded_torus[i][k]);
                            // fprintf(out_file, ", %d", folded_torus[i][k]);
                        }
                        printf("\n");
                        // fprintf(out_file, "\n");
                    }
                // }
            }     
        }

        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        // fprintf(out_file, ", %d", folded_torus[n][j]);
                    }  
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
                else
                {
                    count = 0;
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        // fprintf(out_file, ", %d", folded_torus[n][j]);
                    }
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
            // }
        } 
#endif
    routing_folded_torus(n, m, folded_torus);
    return folded_torus[head_node_n][head_node_m];
}



/** @fn int routing_mesh(int n, int m, int mesh[n][m])
 * @brief 
 * @param int n
 * @param int m
 * @param int mesh[n][m]
 */
int routing_mesh(int n, int m, int mesh[n][m])
{
    int src, dest;
    int src_row = 0, src_col = 0, dest_row = 0, dest_col = 0;
    printf("\n Inside the routing Mesh\n");

    printf("Enter the source and Destination");
    scanf("%d %d", &src, &dest);

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            // printf("%d\t", mesh[i][j]);  
            // Finding the position of the source and destination
            if (src == mesh[i][j])
            {
                printf("\n The src location:[%d] [%d]", i, j);
                src_row = i;
                src_col = j;
            }
            if (dest == mesh[i][j])
            {
                printf("\n The dest location:[%d] [%d]", i, j);
                dest_row = i;
                dest_col = j;
            }
        }
    }

    if(src > 0 && dest <= mesh[n][m])
    {
        if(src_col <= dest_col) /*SOURCE should be on Left side than DESTINATION*/
        {
            printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, mesh, dest, src_col+k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = mesh[i][src_col+k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        printf("\n X-movenment");
                        path[len] = mesh[src_row][src_col+k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }
        else  /*SOURCE should be on right side than DESTINATION*/
        {
            printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, mesh, dest, src_col-k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = mesh[i][src_col-k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        printf("\n X-movenment");
                        path[len] = mesh[src_row][src_col-k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }   
    }
    else
    {
        printf("\nPlease give source and destination in the range");
    }
    return 0;
}

/** @fn int mesh_network(int n, int m, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int m
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int mesh_network(int n, int m, int id_no, int * head_combination, int start)
{
    int mesh[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {     
            count += 1;
            mesh[i][j] = count;
        }    
    }
    printf("\n");
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("%d\t", mesh[i][j]);  
        }
        printf("\n");
    }
    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }
        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", mesh[head_node_n][head_node_m]);
    // Try to find the combination of the node connected with the other node
#if 0    
    // printf("For First row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
        }
        else if (i == 1)
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
        }
        else
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][m], mesh[1][m-1], mesh[1][m], mesh[1+1][m]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
        }
    }
    // printf("All middle row\n");
    for (int i = 2; i <= n-1; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if (j > 1 & j < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 4", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 4", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j], mesh[i][j+1], mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
            }
            else if (j == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j+1]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
            }
            else
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j-1]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
            }           
        }     
    }
    
    // printf("The Last Row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
        }
        else if (i == 1)
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
        }
        else
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][m], mesh[m][n-1], mesh[n][m], mesh[n-1][m]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
        }
    }  
#endif
    printf("\nSTART finding the path without starvation");

    routing_mesh(n, m, mesh);

    return mesh[head_node_n][head_node_m]; 
}

/** @fn int routing_chain(int n, int chain[n])
 * @brief 
 * @param int n
 * @param int chain[n]
 */
int routing_chain(int n, int chain[n])
{
    int src, dest;
    printf("\n Please Enter the Source and Destination");
    scanf("%d \n %d", &src, &dest);
    if (src > 0 && dest <= chain[n])
    {
        if(dest > src)  //Right Movenment
        {
            printf("\n Path: ");
            for (int i = src; i <= dest; i++)
            {
                printf("%d\t", chain[i]);
            } 
        }
        else            //Left Movenment
        {
            printf("\n Path: ");
            for (int i = src; i >= dest; i--)
            {
                printf("%d\t", chain[i]);
            }          
        }
    }
    else
    {
        printf("\n Please enter the src and dest according to the range");
    }
}

/** @fn int chain_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int chain_network(int n, int id_no, int * head_combination, int start)
{

    int chain[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        chain[i] = i;
    }
    for (int i = 1; i <= n; i++)
    {
        printf("%d\t", chain[i]);
    }
    if (n%2 == 0)
    {
        head_node = n/2;
    }
    else
    {
        head_node = (n+1)/2;
    }
    printf("\nHead Node: %d", chain[head_node]);

    routing_chain(n, chain);

#if 0    
    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
    // fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
    printf("\nThe Node %d Connected to %d", chain[i], chain[i+1]);
    // fprintf(out_file, "\nThe Node %d Connected to %d", chain[i], chain[i+1]);
    for (int i = 2; i < n; i++)
    {
        printf("\n\nNode ID: %d_%d\nLinks: 2", id_no, i);
        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node %d Connected to %d\nThe Node %d Connected to %d", chain[i], chain[i-1], chain[i], chain[i+1]);
        // fprintf(out_file, "\nThe Node %d Connected to %d, %d", chain[i], chain[i-1], chain[i+1]);
        
    }
    i = n;
    printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
    // fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
    printf("\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
    // fprintf(out_file, "\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
#endif

    return chain[head_node];

}

/** @fn int routing_ring(int n, int ring[n])
 * @brief 
 * @param int n
 * @param int ring[n]
 */
int routing_ring(int n, int ring[n])
{
    int src, dest, deciding = 0, temp = 0;
    printf("\n Please Enter the Source and Destination");
    scanf("%d \n %d", &src, &dest);
    deciding = n/2;
    if (src > 0 && dest <= ring[n])
    {
        printf("\n Path: ");
        if(dest > src)  
        {
            if ((dest - src) < deciding)  // clockwise Movenment
            {
                for (int i = src; i <= dest; i++)
                {
                    if(ring[i] == dest+1)
                        break;
                    else
                        printf("%d\t", ring[i]);
                    
                }  
            }
            if ((dest - src) > deciding)  // Anti-Clockwise Movenment   
            {
                for (int i = 0; i < dest-src; i++)
                {
                    if ((src - i) > 0)
                    {
                        printf("%d\t", ring[src - i]);
                    }
                    else if ((src - i) <= 0)
                    {
                        if(ring[n - temp] == dest-1)
                            break;
                        else
                        {
                            printf("%d\t", ring[n - temp]);
                            temp = temp + 1;
                        }    
                    }
                }
                
            }
        }
        else
        { 
            printf("\n In else");
            if ((src - dest) < deciding)  // Anti-Clockwise Movenment   
            {
                for (int i = 0; i < src-dest+1; i++)
                {
                    printf("Hllo%d\t", ring[src - i]);
                }
            }
            if ((src - dest) > deciding)  // Clockwise Movenment   
            {
                for (int i = 0; i < src-dest; i++)
                {
                    // printf("%d\t", ring[src - i]);
                    if ((src + i) <= n)
                    {
                        printf("Hey%d:\t", ring[src + i]);
                    }
                    else 
                    // if ((src + i) <= n)
                    {
                        if(ring[1 + temp] == dest+1)
                            break;
                        else
                        {
                            printf("Hi%d\t", ring[1 + temp]);
                            temp = temp + 1;

                        }
                    }
                }
                
            }
        }
    }
    else
    {
        printf("\n Please enter the src and dest according to the range");
    }
}

/** @fn int ring_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int ring_network(int n, int id_no, int * head_combination, int start)
{
    int ring[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        ring[i] = i;
    }
    for (int i = 1; i <= n; i++)
    {
        printf("%d\t", ring[i]);
    }
    srand(time(0));
    head_node = (rand() % (n + 1)) + 0;
    printf("\nHead Node: %d", ring[head_node]);

    routing_ring(n, ring);

#if 0    
    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
    printf("\nThe Node: %d Connected to: %d\nThe Node: %d Connected to: %d", ring[i], ring[i+1], ring[i], ring[n]);
    // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i+1], ring[n]);
    for (int i = 2; i < n; i++)
    {
        // if(i == head_node)
        //     continue;
        // else
        // {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe Node: %d Connected to %d\nThe Node: %d Connected to %d", ring[i], ring[i-1], ring[i], ring[i+1]);
            // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i-1], ring[i+1]);
        // }
    }
    i = n;
    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
    printf("\nThe Node %d Connected to: %d\nThe Node %d Connected to: %d\n", ring[i], ring[i-1], ring[i], ring[0]);
    // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d\n", ring[i], ring[i-1], ring[0]);
#endif
    return ring[head_node];

}

/** @fn int length_binary(int n)
 * @brief 
 * @param int n
 */
int length_binary(int n)
{
    int count = 0;
    do 
    {
        n /= 10;
        ++count;
    } while (n != 0);

    return count;
}

/** @fn int *decimal_to_binary(int decimalnum)
 * @brief 
 * @param int decimalnum
 */
int *decimal_to_binary(int decimalnum)
{
    int binarynum = 0;
    int rem, temp = 1;
    static int arr[3];

    while (decimalnum!=0)
    {
        rem = decimalnum%2;
        decimalnum = decimalnum / 2;
        binarynum = binarynum + rem*temp;
        temp = temp * 10;
    }
    memset(arr, 0, 3);
    // return binarynum;
    // printf("\nbinarynum: %d", binarynum);
    // printf("\nlength_binary(binarynum): %d", length_binary(binarynum));
    if (length_binary(binarynum) == 1)
    {
        for (int i = 0; i < 3-length_binary(binarynum); i++)
        {
            arr[i] = 0;
        }
        arr[3-length_binary(binarynum)] = binarynum;
    }
    if (length_binary(binarynum) == 2)
    {
        for (int i = 0; i < 3-length_binary(binarynum); i++)
        {
            arr[i] = 0;
        }
        arr[2] = binarynum % 10;
        arr[1] = binarynum / 10;
    }
    
    if (length_binary(binarynum) == 3)
    {
        int temp = binarynum % 10;
        binarynum = binarynum / 10;

        arr[0] = binarynum / 10;
        arr[1] = binarynum % 10;
        arr[2] = temp;
    }
    // printf("\n");
    // for (int i = 0; i < 3; i++)
    // {
    //     printf("%d", arr[i]);
    // }
    // printf("\n");
    
    return arr;
}

/** @fn int routing_hypercube()
 * @brief 
 */
int routing_hypercube()
{
    int src, dest, path = 0;
    int *src_array, *dest_array, temp_array[3], temp[3];
    printf("\nPlz enter Source and Destination");
    scanf("%d \n %d", &src, &dest);

    if (src >= 0 && dest <= 8)
    {
        dest_array = decimal_to_binary(dest);
        for (int i = 0; i < 3; i++)
        {
            temp[i] = dest_array[i];
            temp_array[i] = dest_array[i];
        }
        src_array = decimal_to_binary(src);
        for (int i = 0; i < 3; i++)
        {
            if(src_array[i] != temp_array[i])
                path = path + 1; 
        }
        // printf("\nPATH: %d", path);
        printf("\nPATH:");
        for (int i = 0; i < 3; i++)
        {
            printf("%d", src_array[i]);
        }
        printf("\t");
        if (path == 2)
        {
            if (temp_array[2] == 0)
            {
                temp_array[2] = 1;
            }
            if (temp_array[2] == 1)
            {
                temp_array[2] = 0;
            }
            for (int i = 0; i < 3; i++)
            {
                printf("%d", temp_array[i]);
            }
            printf("\t");
        }
        else if(path == 3)
        {
            for (int i = 0; i < 3; i++)
            {
                temp_array[i] = temp_array[i];
            }
            for (int i = 1; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    temp_array[j] = temp[j];
                }
                if (i == 1)
                {
                    if (temp_array[i] == 1)
                    {
                        temp_array[i] = 0;
                    }
                    else
                    {
                        temp_array[i] = 1;
                    }

                    if (temp_array[i+1] == 1)
                    {
                        temp_array[i+1] = 0;
                    }
                    else
                    {
                        temp_array[i+1] = 1;
                    }

                }
                if (i == 2)
                {
                    if (temp_array[i] == 1)
                    {
                        temp_array[i] = 0;
                    }
                    else
                    {
                        temp_array[i] = 1;
                    }

                }
                for (int i = 0; i < 3; i++)
                {
                    printf("%d", temp_array[i]);
                } 
            printf("\t");
            }
            
        }
        // printf("\ndest:");
        for (int i = 0; i < 3; i++)
        {
            printf("%d", temp[i]);
        }    
        printf("\t");
    }
    else
        printf("\n Plz enter within Range");
}

/** @fn int hypercube_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int hypercube_network(int n, int id_no, int * head_combination, int start)
{
    int *array;
    for (int i = 0; i < n; i++)
    {
        array = decimal_to_binary(i);
        // for (int i = 0; i < 3; i++)
        // {
        //     printf("%d", array[i]);
        // }
        // printf("\n");
    }
    routing_hypercube();
}

/** @fn int butterfly_network(int n, int i)
 * @brief 
 * @param int n
 * @param int i
 */
int butterfly_network(int n, int i)
{
    int m = 0;
    m = n/2;
    int butterfly[n][m];
    int path_1[n][6], path_2[n][6], path_3[n][6], final_path[n][50];
    int count = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {     
            count += 1;
            butterfly[i][j] = count;
        }    
    }
    printf("\n");
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("[%d][%d]%d \t",i, j, butterfly[i][j]);  
        }
        printf("\n");
    }

    /* 1st Coloumn */
    for (int i = 1; i <= n; i++)
    {
        path_1[i][1] = butterfly[i][1]; 
        path_1[i][2] = butterfly[i][2];
        if (i <= m)
        {
            path_1[i][3] = butterfly[i+m][2];
        }
        else
            path_1[i][3] = butterfly[i-m][2];  
    }
    //PRINT
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= 3; j++)
        {
            printf("%d\t", path_1[i][j]);
        }
        printf("\n");
    }
    printf("\n");


    /* 2nd Coloumn */
    m = n/2;
    int temp = m/2;
    for (int i = 1; i <= n; i++)
    {
        path_2[i][1] = butterfly[i][2];
        path_2[i][2] = butterfly[i][3];
        if (i <= m)
        {
            if (i <= temp)
            {
                path_2[i][3] = butterfly[i+temp][3];
            }
            else
                path_2[i][3] = butterfly[i-temp][3];
        }
        else
        {
            if ((i+temp) <= 6)
            {
                path_2[i][3] = butterfly[i+temp][3]; 
            }
            else
            {
                path_2[i][3] = butterfly[i-temp][3];
            }  
        }
    }
    // PRINT
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= 3; j++)
        {
            printf("%d\t", path_2[i][j]);
        }
        printf("\n");
    }
    printf("\n");


    /* 3rd coloumn */
    m = m/2;
    for (int i = 1; i <= n; i++)
    {
        path_3[i][1] = butterfly[i][3];
        path_3[i][2] = butterfly[i][4];
        if (i % 2 != 0)
        {
            path_3[i][3] = butterfly[i+1][4];
        }
        else
        {
            path_3[i][3] = butterfly[i-1][4];
        }  
    }
    // PRINT
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j <= 3; j++)
        {
            printf("%d\t", path_3[i][j]);
        }
        printf("\n");
    }


    for (int i = 1; i <=n ; i++)
    {
        count = 1;
        for (int j = 1; j <= 3; j++)
        {
            final_path[i][count] = path_1[i][j];
            count = count + 1;
        }
        for (int k = 2; k <= 3; k++)
        {
            for (int j = 1; j <= n; j++)
            {
                if(final_path[i][k] == path_2[j][1])
                {
                    for (int l = 2; l <=3; l++)
                    {
                        final_path[i][count] = path_2[j][l];
                        count = count + 1;
                    } 
                }
            }
        }

        for (int k = 4; k <= 5; k++)
        {
            for (int j = 1; j <= n; j++)
            {
                if(final_path[i][k] == path_3[j][1])
                {
                    for (int l = 2; l <=3; l++)
                    {
                        final_path[i][count] = path_3[j][l];
                        count = count + 1;
                    } 
                }
            }
        }
    }
    printf("count: %d\n", count);
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j < count; j++)
        {
            printf("%d\t", final_path[i][j]);
        }
        printf("\n");
    }
    

}

/** @fn void main()
 * @brief 
 *        C for Chain Topology
 *        R for Ring Topology
 *        M for Mesh Topology
 *        F for Folded Torus Topology
 *        H for Hypercube Topology
 *        B for Butterfly Tolpology
 *
 */
void main()
{
    char ch;
    int n = 0 , m = 0, headnode = 0;
    printf("\nPlz enter the L1 Network");
    printf("\n C for Chain\n R for Ring\n M for Mesh\n F for Folded Torus\n H for Hypercube\n B for Butterfly\n");
    scanf("%c", &ch);

    switch (ch)
    {
        case 'C':
            printf("\n==========Chain==========\n");
            // n = node_value(strlen(read_L1), read_L1);
            printf("Enter the node n");
            scanf("%d", &n);
            // printf("Id number: %d\n", i);
            // fprintf(out_file, "\nChain Network: Id Number: %d", i);
            headnode = chain_network(n, 0, 0, 0);
            // Head_node[i] = headnode;
        break;

        case 'R':
            printf("\n==========Ring==========\n");
            // n = node_value(strlen(read_L1), read_L1);
            printf("Enter the node n");
            scanf("%d", &n);
            // printf("Id number: %d\n", i);
            // fprintf(out_file, "\nRing Network: Id Number: %d", i);
            headnode = ring_network(n, 0, 0, 0);
            // Head_node[i] = headnode;
        break; 

        case 'M':
            printf("\n==========Mesh==========\n");
            // n = node_value(strlen(read_L1), read_L1);
            // m = node_value_m(strlen(read_L1), read_L1);
            printf("Enter the node n and m");
            scanf("%d \n %d", &n, &m);
            headnode = mesh_network(n, m, 0, 0, 0);
            // printf("Id number: %d\n", i);
            // fprintf(out_file, "\nMesh Network: Id Number: %d\n", i);
            printf("\n The length of the path:%d", len);
            printf("\nThe PATH:");
            for (int i = 0; i < len; i++)
            {
                printf("\t%d", path[i]);
            }
            
        break; 

        case 'F':
            printf("\n==========Folded Torus==========\n");
            printf("\nFolded Torus\n");
            // n = node_value(strlen(read_L1), read_L1);
            // m = node_value_m(strlen(read_L1), read_L1);
            printf("Enter the node n and m");
            scanf("%d \n %d", &n, &m);
            headnode = folded_torus_network(n, m, 0, 0, 0);
            // fprintf(out_file, "\nFolded Torus Network: Id Number: %d", i);
        break;      

        case 'H':
            printf("\n==========Hypercube==========\n");
            printf("The size defined to the Hypercube is 8\n");
            // fprintf(out_file, "\nHypercube Network: Id Number: %d", i);
            headnode = hypercube_network(8, 0, 0, 0);
        break; 

        case 'B':
            printf("\n==========Butterfly==========\n");
            printf("\nPlz Enter k if the configuration is 2^k*2^k\n");
            printf("Enter the node n");
            scanf("%d", &n);
            // n = node_value(strlen(read_L1), read_L1);
            // fprintf(out_file, "\nButterfly Network: Id Number: %d", i);
            // printf("Id number: %d\n", i);
            butterfly_network(n, 0);
        break;

        default:
        break;          
    }
}
