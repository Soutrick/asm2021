/***************************************************************************
*
* Project           		    :  PROJECT-2
* Name of the file	     	    :  project2.c
* Brief Description of file     :  
* Name of Author    	        :  Soutrick Roy Chowdhury
* Email ID                      :  soutrickofficial@gmail.com
*
***************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>
#include <string.h>

FILE *out_file;
FILE *L0Topology;
FILE *L1Topology;

int  SRC_PATH[50], DEST_PATH[50]; 

/** @fn int check_in_col(int n, int m, int mesh_torus[n][m], int dest, int col)
 * @brief 
 * @param int n
 * @param int m
 * @param int mesh_torus[n][m]
 * @param int col
 */
int check_in_col(int n, int m, int mesh_torus[n][m], int dest, int col)
{
    // printf("\n In check_in_col");
    for (int i = 1; i <= n; i++)
    {
        if (dest == mesh_torus[i][col])
        {
            // printf("\n Y-movenment");
            return 1;
        }
    }
}

/** @fn int routing_folded_torus(int n, int m, int folded_torus[n][m])
 * @brief 
 * @param int n
 * @param int m
 * @param int folded_torus[n][m]
 */
int routing_folded_torus(int n, int m, int folded_torus[n][m], int head_node, int id_no, int decision)
{
    int src, dest;
    int src_row = 0, src_col = 0, dest_row = 0, dest_col = 0;
    int path[50], len = 0;
    // printf("\n Inside the routing folded_torus\n");

    // printf("Enter the source and Destination");
    // scanf("%d %d", &src, &dest);
    if (decision == 0)
    {
        printf("\nPlz enter Source and Destination: ");
        scanf("%d \n %d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = head_node;
    }
    else
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = head_node;
    }

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            // printf("%d\t", mesh[i][j]);  
            // Finding the position of the source and destination
            if (src == folded_torus[i][j])
            {
                // printf("\n The src location:[%d] [%d]", i, j);
                src_row = i;
                src_col = j;
            }
            if (dest == folded_torus[i][j])
            {
                // printf("\n The dest location:[%d] [%d]", i, j);
                dest_row = i;
                dest_col = j;
            }
        }
    }

    if(src > 0 && dest <= folded_torus[n][m])
    {
        if(src_col <= dest_col) /*SOURCE should be on Left side than DESTINATION*/
        {
            // printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, folded_torus, dest, src_col+k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = folded_torus[i][src_col+k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        // printf("\n X-movenment");
                        path[len] = folded_torus[src_row][src_col+k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }
        else  /*SOURCE should be on right side than DESTINATION*/
        {
            // printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, folded_torus, dest, src_col-k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = folded_torus[i][src_col-k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        // printf("\n X-movenment");
                        path[len] = folded_torus[src_row][src_col-k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }   
    }
    else
    {
        printf("\nPlease give source and destination in the range");
    }
    // printf("\nThe PATH:");
    if (decision == 1)
        {
            for (int i = 0; i < len; i++)
            {
                SRC_PATH[i] = path[i];
            }
            printf("\nSource Path: ");
            for (int i = 0; i < len; i++)
            {
                printf("%d_%d\t", id_no, SRC_PATH[i]);
            }
        }
        else if(decision == 2)
        {
            for (int i = 0; i < len; i++)
            {
                DEST_PATH[i] = path[i];
            }
            printf("\nDestination Path: ");
            for (int i = 0; i <len; i++)
            {
                printf("%d_%d\t", id_no, DEST_PATH[i]);
            }
        }
        else if(decision == 0)
        {
            for (int i = 0; i < len; i++)
            {
                printf("\t%d\t", path[i]);
            }
        }
    
    return 0;
}

/** @fn int folded_torus_network(int n, int m, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n 
 * @param int m
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int folded_torus_network(int n, int m, int id_no, int * head_combination, int decision)
{
    int folded_torus[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            count += 1;
            folded_torus[i][j] = count;
        }    
    }
    // printf("\n");
    // for (int i = 1; i <= n; i++)    
    // {    
    //     for (int j = 1; j <= m; j++)    
    //     {    
    //         printf("%d_%d\t",id_no, folded_torus[i][j]);  
    //     }
    //     printf("\n");
    // }
    
    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }
        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", folded_torus[head_node_n][head_node_m]);
    
#if 0

        // printf("For First row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[i][j]);
                        // fprintf(out_file, ", %d", folded_torus[i][j]);
                    }  
                    // fprintf(out_file, "\n");
                }
                else
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[1][i], folded_torus[n][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[1][i], folded_torus[1+1][i], folded_torus[n][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[1][i], folded_torus[1][j]);
                        // fprintf(out_file, ", %d", folded_torus[1][j]);
                    }
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
            // }
            
        }

        // printf("All middle row\n");
        for (int i = 2; i <= n-1; i++)
        {
            for (int j = 1; j <= m; j++)
            {
                // if (i == head_node_n && j == head_node_m)
                // {
                //     continue;
                // }
                // else
                // {
                    if (j > 1 & j < m)
                    {
                        printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                    }
                    else if (j == 1)
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = 3; k <= m; k=k+2)
                        {
                            printf("\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i][k]);
                            // fprintf(out_file, ", %d", folded_torus[i][k]);
                        }  
                        printf("\n");
                        // fprintf(out_file, "\n");
                    }
                    else
                    {
                        printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                        printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i][j], folded_torus[i+1][j]);
                        // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[i][j], folded_torus[i-1][j], folded_torus[i+1][j]);
                        for (int k = m-2; k >= 1; k=k-2)
                        {
                            printf("\nThe node %d connected to the %d",folded_torus[i][j], folded_torus[i][k]);
                            // fprintf(out_file, ", %d", folded_torus[i][k]);
                        }
                        printf("\n");
                        // fprintf(out_file, "\n");
                    }
                // }
            }     
        }

        // printf("The Last Row\n");
        for (int i = 1; i <= m; i++)
        {
            // if (i == head_node_m)
            // {
            //     continue;
            // }

            // else
            // {
                if (i > 1 & i < m)
                {
                    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                }
                else if (i == 1)
                {
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = 3; j <= m; j=j+2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        // fprintf(out_file, ", %d", folded_torus[n][j]);
                    }  
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
                else
                {
                    count = 0;
                    printf("\nNode ID: %d_%d\nLinks: ", id_no, i);
                    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: ", id_no, i);
                    printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", folded_torus[n][i], folded_torus[n-1][i], folded_torus[n][i], folded_torus[1][i]);
                    // fprintf(out_file, "The node %d connected to the %d, %d", folded_torus[n][i], folded_torus[n-1][i], folded_torus[1][i]);
                    for (int j = m-2; j >= 1; j=j-2)
                    {
                        printf("\nThe node %d connected to the %d",folded_torus[n][i], folded_torus[n][j]);
                        // fprintf(out_file, ", %d", folded_torus[n][j]);
                    }
                    printf("\n");
                    // fprintf(out_file, "\n");
                }
            // }
        } 
#endif
    routing_folded_torus(n, m, folded_torus, folded_torus[head_node_n][head_node_m], id_no, decision);
    return folded_torus[head_node_n][head_node_m];
}


/** @fn int routing_mesh(int n, int m, int mesh[n][m])
 * @brief 
 * @param int n
 * @param int m
 * @param int mesh[n][m]
 */
int routing_mesh(int n, int m, int mesh[n][m], int head_node, int id_no, int decision)
{
    int src, dest;
    int src_row = 0, src_col = 0, dest_row = 0, dest_col = 0;
    int path[50], len = 0;
    // printf("\n Inside the routing Mesh\n");

    // printf("Enter the source and Destination");
    // scanf("%d %d", &src, &dest);
    if (decision == 0)
    {
        printf("\nPlz enter Source and Destination: ");
        scanf("%d \n %d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = head_node;
    }
    else
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = head_node;
    }

    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            // printf("%d\t", mesh[i][j]);  
            // Finding the position of the source and destination
            if (src == mesh[i][j])
            {
                // printf("\n The src location:[%d] [%d]", i, j);
                src_row = i;
                src_col = j;
            }
            if (dest == mesh[i][j])
            {
                // printf("\n The dest location:[%d] [%d]", i, j);
                dest_row = i;
                dest_col = j;
            }
        }
    }

    if(src > 0 && dest <= mesh[n][m])
    {
        if(src_col <= dest_col) /*SOURCE should be on Left side than DESTINATION*/
        {
            // printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, mesh, dest, src_col+k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = mesh[i][src_col+k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        // printf("\n X-movenment");
                        path[len] = mesh[src_row][src_col+k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }
        else  /*SOURCE should be on right side than DESTINATION*/
        {
            // printf("\n In if \n Len: %d", len);
            for (int k = 0; k < m; k++)
            {
                if(path[len] != dest)
                {
                    if(check_in_col(n, m, mesh, dest, src_col-k) == 1)
                    {
                        for (int i = src_row; i <= dest_row; i++)
                        {
                            path[len] = mesh[i][src_col-k];
                            len = len + 1;
                        }
                        break;
                    }
                    else
                    {
                        // printf("\n X-movenment");
                        path[len] = mesh[src_row][src_col-k];
                        len = len + 1;
                    }  
                }
                else
                    break; 
            }
        }   
    }
    else
    {
        printf("\nPlease give source and destination in the range");
    }
    // printf("\nThe PATH:");
    if (decision == 1)
        {
            for (int i = 0; i < len; i++)
            {
                SRC_PATH[i] = path[i];
            }
            printf("\nSource Path: ");
            for (int i = 0; i < len; i++)
            {
                printf("%d_%d\t", id_no, SRC_PATH[i]);
            }
        }
        else if(decision == 2)
        {
            for (int i = 0; i < len; i++)
            {
                DEST_PATH[i] = path[i];
            }
            printf("\nDestination Path: ");
            for (int i = 0; i < len; i++)
            {
                printf("%d_%d\t", id_no, DEST_PATH[i]);
            }
        }
        else if (decision == 0)
        {
            for (int i = 0; i < len; i++)
            {
                printf("\t%d_%d\t", id_no, path[i]);
            }
        }
    return 0;
}

/** @fn int mesh_network(int n, int m, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int m
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int mesh_network(int n, int m, int id_no, int * head_combination, int decision)
{
    int mesh[n][m];
    int count = 0, head_node_n = 0, head_node_m = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {     
            count += 1;
            mesh[i][j] = count;
        }    
    }
    printf("\n");
    // for (int i = 1; i <= n; i++)    
    // {    
    //     for (int j = 1; j <= m; j++)    
    //     {    
    //         printf("%d_%d\t",id_no, mesh[i][j]);  
    //     }
    //     printf("\n");
    // }
    if (n == m)
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
            head_node_m = m/2;
        }
        else
        {
            head_node_n = (n+1)/2;
            head_node_m = (m+1)/2;
        }
    }
    else
    {
        if (n%2 == 0)
        {
            head_node_n = n/2;
        }
        else
        {
            head_node_n = (n+1)/2;
        }
        if (m%2 == 0)
        {
            head_node_m = m/2;
        }
        else
        {
            head_node_m = (m+1)/2;
        }
    }
    printf("The Head Node: %d\n", mesh[head_node_n][head_node_m]);
    // Try to find the combination of the node connected with the other node
#if 0    
    // printf("For First row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[1][i], mesh[1][i-1], mesh[1][i+1], mesh[1+1][i]);
        }
        else if (i == 1)
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][i], mesh[1][i+1], mesh[1][i], mesh[1+1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][i], mesh[1][i+1], mesh[1+1][i]);
        }
        else
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[1][m], mesh[1][m-1], mesh[1][m], mesh[1+1][m]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[1][m], mesh[1][m-1], mesh[1+1][m]);
        }
    }
    // printf("All middle row\n");
    for (int i = 2; i <= n-1; i++)
    {
        for (int j = 1; j <= m; j++)
        {
            if (j > 1 & j < m)
            {
                printf("\nNode ID: %d_%d\nLinks: 4", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 4", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j], mesh[i][j+1], mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d, %d\n", mesh[i][j], mesh[i][j-1], mesh[i][j+1], mesh[i-1][j], mesh[i+1][j]);
            }
            else if (j == 1)
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j+1]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j+1]);
            }
            else
            {
                printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
                // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
                printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[i][j], mesh[i-1][j], mesh[i][j], mesh[i+1][j], mesh[i][j], mesh[i][j-1]);
                // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[i][j], mesh[i-1][j], mesh[i+1][j], mesh[i][j-1]);
            }           
        }     
    }
    
    // printf("The Last Row\n");
    for (int i = 1; i <= m; i++)
    {
        if (i > 1 & i < m)
        {
            printf("\nNode ID: %d_%d\nLinks: 3", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 3", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d, %d\n", mesh[n][i], mesh[n][i-1], mesh[n][i+1], mesh[n-1][i]);
        }
        else if (i == 1)
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][i], mesh[n][i+1], mesh[n][i], mesh[n-1][i]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][i], mesh[n][i+1], mesh[n-1][i]);
        }
        else
        {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe node %d connected to the %d\nThe node %d connected to the %d\n", mesh[n][m], mesh[m][n-1], mesh[n][m], mesh[n-1][m]);
            // fprintf(out_file, "The node %d connected to the %d, %d\n", mesh[n][m], mesh[m][n-1], mesh[n-1][m]);
        }
    }  
#endif
    // printf("\nSTART finding the path without starvation");

    routing_mesh(n, m, mesh, mesh[head_node_n][head_node_m], id_no, decision);

    return mesh[head_node_n][head_node_m]; 
}

/** @fn int routing_chain(int n, int chain[n])
 * @brief 
 * @param int n
 * @param int chain[n]
 */
int routing_chain(int n, int chain[n], int head_node, int id_no, int decision)
{
    int src, dest;
    int counter = 0;
    // printf("\n Please Enter the Source and Destination");
    // scanf("%d \n %d", &src, &dest);
    if (decision == 0)
    {
        printf("\nPlz enter Source and Destination: ");
        scanf("%d \n %d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = head_node;
    }
    else if(decision == 2)
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = head_node;
    }

    if (src > 0 && dest <= chain[n])
    {
        if(dest > src)  //Right side Movenment
        {
            // printf("\n Path: ");
            if (decision == 1)
            {
                printf("\nSource Path: ");
                for (int i = src; i <= dest; i++)
                {
                    SRC_PATH[++counter] = chain[i];
                }
                for (int i = 1; i <= counter; i++)
                {
                    printf("%d_%d\t", id_no, SRC_PATH[i]);
                }
            }
            else if(decision == 2)
            {
                printf("\nDestination Path: ");
                for (int i = src; i <= dest; i++)
                {
                    DEST_PATH[++counter] = chain[i];
                }
                for (int i = 1; i <= counter; i++)
                {
                    printf("%d_%d\t", id_no, DEST_PATH[i]);
                }
            }
            else if(decision == 0)
            {
                for (int i = src; i <= dest; i++)
                {
                   printf("%d_%d\t", id_no, chain[i]);
                } 
            }
        }
        else            //Left side Movenment
        {
            // printf("\n Path: ");
            if (decision == 1)
            {
                printf("\nSource Path: ");
                for (int i = src; i >= dest; i--)
                {
                    SRC_PATH[++counter] = chain[i];
                }
                for (int i = 1; i <= counter; i++)
                {
                    printf("%d_%d\t", id_no, SRC_PATH[i]);
                }
            }
            else if(decision == 2)
            {
                printf("\nDestination Path: ");
                for (int i = src; i >= dest; i--)
                {
                    DEST_PATH[++counter] = chain[i];
                }
                for (int i = 1; i <= counter; i++)
                {
                    printf("%d_%d\t", id_no, DEST_PATH[i]);
                }
            }
            else if(decision == 0)
            {
                for (int i = src; i >= dest; i--)
                {
                    printf("%d_%d\t", id_no, chain[i]);
                }          
            }
            
        }
    }
    else
    {
        printf("\n Please enter the src and dest according to the range");
    }
}

/** @fn int chain_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int chain_network(int n, int id_no, int * head_combination, int decision)
{

    int chain[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        chain[i] = i;
    }
    // for (int i = 1; i <= n; i++)
    // {
    //     printf("%d_%d\t",id_no, chain[i]);
    // }
    if (n%2 == 0)
    {
        head_node = n/2;
    }
    else
    {
        head_node = (n+1)/2;
    }
    printf("\nHead Node: %d", chain[head_node]);

    routing_chain(n, chain, chain[head_node], id_no, decision);

#if 0    
    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
    // fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
    printf("\nThe Node %d Connected to %d", chain[i], chain[i+1]);
    // fprintf(out_file, "\nThe Node %d Connected to %d", chain[i], chain[i+1]);
    for (int i = 2; i < n; i++)
    {
        printf("\n\nNode ID: %d_%d\nLinks: 2", id_no, i);
        // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
        printf("\nThe Node %d Connected to %d\nThe Node %d Connected to %d", chain[i], chain[i-1], chain[i], chain[i+1]);
        // fprintf(out_file, "\nThe Node %d Connected to %d, %d", chain[i], chain[i-1], chain[i+1]);
        
    }
    i = n;
    printf("\nNode ID: %d_%d\nLinks: 1", id_no, i);
    // fprintf(out_file, "\n\nNode ID: %d_%d\nLinks: 1", id_no, i);
    printf("\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
    // fprintf(out_file, "\nThe Node %d Connected to %d\n", chain[i], chain[i-1]);
#endif

    return chain[head_node];

}

/** @fn int routing_ring(int n, int ring[n])
 * @brief 
 * @param int n
 * @param int ring[n]
 */
int routing_ring(int n, int ring[n], int head_node, int id_no, int decision)
{
    int src, dest, deciding = 0, temp = 0;
    int PATH[100], counter = 0;
    // printf("\n Please Enter the Source and Destination");
    // scanf("%d \n %d", &src, &dest);
    if (decision == 0)
    {
        printf("\nPlz enter Source and Destination: ");
        scanf("%d \n %d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = head_node;
    }
    else
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = head_node;
    }

    deciding = n/2;
    if (src > 0 && dest <= ring[n])
    {
        // printf("\n Path: ");
        {
            if(dest > src)  
            {
                if ((dest - src) < deciding)  // clockwise Movenment
                {
                    for (int i = src; i <= dest; i++)
                    {
                        if(ring[i] == dest+1)
                            break;
                        else
                        {
                            PATH[++counter] = ring[i];
                            // printf("%d\t", ring[i]);
                        }
                    }  
                }
                if ((dest - src) > deciding)  // Anti-Clockwise Movenment   
                {
                    for (int i = 0; i < dest-src; i++)
                    {
                        if ((src - i) > 0)
                        {
                            PATH[++counter] = ring[src - i];
                            // printf("%d\t", ring[src - i]);
                        }
                        else if ((src - i) <= 0)
                        {
                            if(ring[n - temp] == dest-1)
                                break;
                            else
                            {
                                PATH[++counter] = ring[n - temp];
                                // printf("%d\t", ring[n - temp]);
                                temp = temp + 1;
                            }    
                        }
                    }
                }
            }
            else
            { 
                // printf("\n In else");
                if ((src - dest) < deciding)  // Anti-Clockwise Movenment   
                {
                    for (int i = 0; i < src-dest+1; i++)
                    {
                        PATH[++counter] = ring[src - i];
                        // printf("%d\t", ring[src - i]);
                    }
                }
                if ((src - dest) > deciding)  // Clockwise Movenment   
                {
                    for (int i = 0; i < src-dest; i++)
                    {
                        // printf("%d\t", ring[src - i]);
                        if ((src + i) <= n)
                        {
                            PATH[++counter] = ring[src + i];
                            // printf("%d:\t", ring[src + i]);
                        }
                        else 
                        // if ((src + i) <= n)
                        {
                            if(ring[1 + temp] == dest+1)
                            break;
                            else
                            {
                                PATH[++counter] = ring[1 + temp];
                                // printf("%d\t", ring[1 + temp]);
                                temp = temp + 1;

                            }
                        }
                    }  
                }
            }
        }
        PATH[++counter] = 00;

        for (int i = 1; i < counter; i++)
        {
            if (decision == 1)
            {
                if(PATH[i] == 00)
                break;
                else
                {
                    // printf("%d", PATH[i]);
                    SRC_PATH[i] = PATH[i];
                }
            }
            else if(decision == 2)
            {
                if(PATH[i] == 00)
                break;
                else
                {
                    // printf("%d", PATH[i]);
                    DEST_PATH[i] = PATH[i];
                }
            }
            else if( decision == 0)
            {
                if(PATH[i] == 00)
                    break;
                else
                    printf("%d", PATH[i]);
            }
        }

        /* printing the value*/
        if (decision == 1)
        {
            printf("\nSource Path: ");
            for (int i = 1; i < counter; i++)
            {
                printf("\t%d_%d",id_no, SRC_PATH[i]);
            }
        }
        else if(decision == 2)
        {
            printf("\nDest Path: ");
            for (int i = 1; i < counter; i++)
            {
                printf("\t%d_%d",id_no, DEST_PATH[i]);
            }
        }
        else if( decision == 0)
        {
            for (int i = 1; i < counter; i++)
            {
                printf("\t%d_%d",id_no, PATH[i]);
            }
        }
    }
    else
    {
        printf("\n Please enter the src and dest according to the range");
    }
}

/** @fn int ring_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int ring_network(int n, int id_no, int * head_combination, int decision)
{
    // printf("\n VALUE:%d", n);
    int ring[n];
    int head_node = 0;
    for (int i = 1; i <= n; i++)
    {
        ring[i] = i;
    }
    // for (int i = 1; i <= n; i++)
    // {
    //     printf("%d_%d\t",id_no, ring[i]);
    // }
    srand(time(0));
    head_node = (rand() % (n + 1)) + 0;
    printf("\nThe Head Node: %d", ring[head_node]);

    routing_ring(n, ring, ring[head_node], id_no, decision);

#if 0    
    // Try to find the combination of the node connected with the other node
    int i = 1;
    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
    printf("\nThe Node: %d Connected to: %d\nThe Node: %d Connected to: %d", ring[i], ring[i+1], ring[i], ring[n]);
    // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i+1], ring[n]);
    for (int i = 2; i < n; i++)
    {
        // if(i == head_node)
        //     continue;
        // else
        // {
            printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
            // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
            printf("\nThe Node: %d Connected to %d\nThe Node: %d Connected to %d", ring[i], ring[i-1], ring[i], ring[i+1]);
            // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d", ring[i], ring[i-1], ring[i+1]);
        // }
    }
    i = n;
    printf("\nNode ID: %d_%d\nLinks: 2", id_no, i);
    // fprintf(out_file, "\nNode ID: %d_%d\nLinks: 2", id_no, i);
    printf("\nThe Node %d Connected to: %d\nThe Node %d Connected to: %d\n", ring[i], ring[i-1], ring[i], ring[0]);
    // fprintf(out_file, "\nThe Node: %d Connected to: %d, %d\n", ring[i], ring[i-1], ring[0]);
#endif
    return ring[head_node];

}

/** @fn int length_binary(int n)
 * @brief 
 * @param int n
 */
int length_binary(int n)
{
    int count = 0;
    do 
    {
        n /= 10;
        ++count;
    } while (n != 0);

    return count;
}

/** @fn int *decimal_to_binary(int decimalnum)
 * @brief 
 * @param int decimalnum
 */
int *decimal_to_binary(int decimalnum)
{
    int binarynum = 0;
    int rem, temp = 1;
    static int arr[3];

    while (decimalnum!=0)
    {
        rem = decimalnum%2;
        decimalnum = decimalnum / 2;
        binarynum = binarynum + rem*temp;
        temp = temp * 10;
    }
    memset(arr, 0, 3);
    // return binarynum;
    // printf("\nbinarynum: %d", binarynum);
    // printf("\nlength_binary(binarynum): %d", length_binary(binarynum));
    if (length_binary(binarynum) == 1)
    {
        for (int i = 0; i < 3-length_binary(binarynum); i++)
        {
            arr[i] = 0;
        }
        arr[3-length_binary(binarynum)] = binarynum;
    }
    if (length_binary(binarynum) == 2)
    {
        for (int i = 0; i < 3-length_binary(binarynum); i++)
        {
            arr[i] = 0;
        }
        arr[2] = binarynum % 10;
        arr[1] = binarynum / 10;
    }
    
    if (length_binary(binarynum) == 3)
    {
        int temp = binarynum % 10;
        binarynum = binarynum / 10;

        arr[0] = binarynum / 10;
        arr[1] = binarynum % 10;
        arr[2] = temp;
    }
    // printf("\n");
    // for (int i = 0; i < 3; i++)
    // {
    //     printf("%d", arr[i]);
    // }
    // printf("\n");
    
    return arr;
}

/** @fn int binary_to_decimal(int *binary_number)
 * @brief 
 * @param int *binary_number
 */
int binary_to_decimal(int *binary_number)
{
    int decimalnum = 0, rem, base = 1;
    for (int i = 2; i >= 0; i--)
    {
        // printf("\n==========%d\n", binary_number[i]);
        rem = binary_number[i];
        decimalnum = decimalnum + rem * base;  
        base = base * 2;  
    }
    printf("\t[%d]", decimalnum);
    return decimalnum;
}

/** @fn int routing_hypercube()
 * @brief 
 */
int routing_hypercube(int decision)
{
    int src, dest, path = 0;
    int *src_array, *dest_array, temp_array[3], temp[3];
    int PATH[8],counter = 0;

    if (decision == 0)
    {
        printf("\nPlz enter Source and Destination: ");
        scanf("%d \n %d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = 5; //Headnode is 5 for Hypercube
    }
    else
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = 5; //Headnode is 5 for Hypercube
    }
    

    if (src >= 0 && dest <= 8)
    {
        dest_array = decimal_to_binary(dest);
        for (int i = 0; i < 3; i++)
        {
            temp[i] = dest_array[i];
            temp_array[i] = dest_array[i];
        }
        src_array = decimal_to_binary(src);
        for (int i = 0; i < 3; i++)
        {
            if(src_array[i] != temp_array[i])
            path = path + 1; 
        }
        // printf("\nPATH: %d", path);
        // printf("\nPATH:");
        // binary_to_decimal(src_array);
        PATH[++counter] = binary_to_decimal(src_array);
        for (int i = 0; i < 3; i++)
        {
            printf("%d", src_array[i]);
        }
        printf("\t");
        if (path == 2)
        {
            if (temp_array[2] == 0)
            {
                temp_array[2] = 1;
            }
            if (temp_array[2] == 1)
            {
                temp_array[2] = 0;
            }
            // binary_to_decimal(temp_array);
            PATH[++counter] = binary_to_decimal(temp_array);
            for (int i = 0; i < 3; i++)
            {
                printf("%d", temp_array[i]);
            }
            printf("\t");
        }
        else if(path == 3)
        {
            for (int i = 0; i < 3; i++)
            {
                temp_array[i] = temp[i];
            }
            for (int i = 1; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    temp_array[j] = temp[j];
                }
                if (i == 1)
                {
                    if (temp_array[i] == 1)
                    {
                        temp_array[i] = 0;
                    }
                    else
                    {
                        temp_array[i] = 1;
                    }

                    if (temp_array[i+1] == 1)
                    {
                        temp_array[i+1] = 0;
                    }
                    else
                    {
                        temp_array[i+1] = 1;
                    }

                }
                if (i == 2)
                {
                    if (temp_array[i] == 1)
                    {
                        temp_array[i] = 0;
                    }
                    else
                    {
                        temp_array[i] = 1;
                    }

                }
                // binary_to_decimal(temp_array);
                PATH[++counter] = binary_to_decimal(temp_array);
                for (int i = 0; i < 3; i++)
                {
                    printf("%d", temp_array[i]);
                } 
            printf("\t");
            }
            
        }
        // printf("\ndest:");
        // binary_to_decimal(temp);
        PATH[++counter] = binary_to_decimal(temp);
        for (int i = 0; i < 3; i++)
        {
            printf("%d", temp[i]);
        }    
        printf("\t");
        if (decision == 1)
        {
            for (int i = 1; i <= counter; i++)
            {
                SRC_PATH[i] = PATH[i];
            }
        }
        else if(decision == 2)
        {
            for (int i = 1; i <= counter; i++)
            {
                DEST_PATH[i] = PATH[i];
            }
        }
        
        // for (int i = 1; i <= counter; i++)
        // {
        //     printf("\n======%d\t", PATH[i]);
        // }
        
    }

    else
        printf("\n Plz enter within Range");
}

/** @fn int hypercube_network(int n, int id_no, int * head_combination, int start)
 * @brief 
 * @param int n
 * @param int id_no
 * @param int * head_combination
 * @param int start
 */
int hypercube_network(int n, int id_no, int * head_combination, int decision)
{
    int *array;
    for (int i = 0; i < n; i++)
    {
        array = decimal_to_binary(i);
        printf("%d_", id_no);
        for (int i = 0; i < 3; i++)
        {
            printf("%d",array[i]);
        }
        printf("\t");
    }
    routing_hypercube(decision);
}

/** @fn int routing_butterfly(int n, int count, int final_path[n][count], int decision)
 * @brief 
 * @param int n
 * @param int count
 * @param int final_path[n][count]
 * @param int decision
 */
int routing_butterfly(int n, int count, int count_1, int m, int final_path[n][count], int final_path_1[n][count_1], int butterfly[n][m], int head_node, int decision)
{
    int src, dest, row_src = 0, col_src = 0, row_dest = 0, col_dest = 0;
    printf("\nIn function\n");
    if (decision == 0)
    {
        printf("\n Plz enter Source and destination");
        scanf("%d\n%d", &src, &dest);
    }
    else if (decision == 1)
    {
        printf("\nPlz enter the Source node: ");
        scanf("%d", &src);
        dest = head_node;
    }
    else
    {
        printf("\nPlz enter the Dest node: ");
        scanf("%d", &dest);
        src = head_node;
    }

    /* Finding position of the src row and col */
    for (int i = 1; i <= m; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (src == butterfly[j][i])
            {
                row_src = j;
                col_src = i;
                break;
            }
        }  
    }
    printf("\nrow_src:%d, col_src:%d", row_src, col_src);

    /* Finding position of the dest row and col */
    for (int i = 1; i <= m; i++)
    {
        for (int j = 1; j <= n; j++)
        {
            if (dest == butterfly[j][i])
            {
                row_dest = j;
                col_dest = i;
                break;
            }
        }  
    }
    printf("\nrow_dest:%d, col_dest:%d\n", row_dest, col_dest);

    if(row_src == row_dest)
    {
        // printf("\nIn if");
        if (col_src < col_dest)
        {
            for (int i = col_src; i <= col_dest; i++)
            {
                printf("%d\t", butterfly[row_src][i]);
            }
        }
        else
        {
            for (int i = col_src; i >= col_dest; i--)
            {
                printf("%d\t", butterfly[row_src][i]);
            }
        }
    }
    else
    {
        int row_src_1 = 0, col_src_1 = 0, row_dest_1 = 0, col_dest_1 = 0;
        printf("\nIn else");
        /* Finding position of the src row and col */
        for (int i = 1; i < count; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (src == final_path[j][i])
                {
                    row_src_1 = j;
                    col_src_1 = i;
                    break;
                }
            }  
        }
        printf("\nrow_src:%d, col_src:%d", row_src_1, col_src_1);

        /* Finding position of the dest row and col */
        for (int i = 1; i < count; i++)
        {
            for (int j = 1; j <= n; j++)
            {
                if (dest == final_path[j][i])
                {
                    row_dest_1 = j;
                    col_dest_1 = i;
                    break;
                }
            }  
        }
        printf("\nrow_dest:%d, col_dest:%d\n", row_dest_1, col_dest_1);

        if(row_src_1 == row_dest_1)
        {
            if (col_src_1 < col_dest_1)
            {
                for (int i = col_src_1; i <= col_dest_1; i++)
                {
                    printf("%d\t", final_path[row_src_1][i]);
                }
            }
            else
            {
                for (int i = col_src_1; i >= col_dest_1; i--)
                {
                    printf("%d\t", final_path[row_src_1][i]);
                }
            }
        }
        else
        {
            /* LAST CHOICE */
            int row_src_2 = 0, col_src_2 = 0, row_dest_2 = 0, col_dest_2 = 0;
            printf("\nLAST CHOICE");
            /* Finding position of the src row and col */
            for (int i = 1; i < count_1; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    if (src == final_path_1[j][i])
                    {
                        row_src_2 = j;
                        col_src_2 = i;
                        break;
                    }
                }  
            }
            printf("\nrow_src:%d, col_src:%d", row_src_2, col_src_2);

            /* Finding position of the dest row and col */
            for (int i = 1; i < count_1; i++)
            {
                for (int j = 1; j <= n; j++)
                {
                    if (dest == final_path_1[j][i])
                    {
                        row_dest_2 = j;
                        col_dest_2 = i;
                        break;
                    }
                }  
            }
            printf("\nrow_dest:%d, col_dest:%d\n", row_dest_2, col_dest_2);

            if(row_src_2 == row_dest_2)
            {
                if (col_src_2 < col_dest_2)
                {
                    for (int i = col_src_2; i <= col_dest_2; i++)
                    {
                        printf("%d\t", final_path_1[row_src_2][i]);
                    }
                }
                else
                {
                    for (int i = col_src_2; i >= col_dest_2; i--)
                    {
                        printf("%d\t", final_path_1[row_src_2][i]);
                    }
                }
            }
        }
    }

    // printf("\n");
    // printf("\n");
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j < count; j++)
    //     {
    //         printf("%d\t", final_path[i][j]);
    //     }
    //     printf("\n");
    // }
    // printf("\n");
    // printf("\n");
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j < count_1; j++)
    //     {
    //         printf("%d\t", final_path_1[i][j]);
    //     }
    //     printf("\n");
    // }
}

/** @fn int butterfly_network(int n, int i)
 * @brief 
 * @param int n
 * @param int i
 */
int butterfly_network(int n, int id_no, int decision)
{
    int m = 0;
    m = n/2;
    int butterfly[n][m];
    int path_1[n][6], path_12[n][6], path_2[n][6], path_3[n][6], final_path[n][5], final_path_1[n][12];
    int count = 0, head_node_n = 0, head_node_m = 0;
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {     
            count += 1;
            butterfly[i][j] = count;
        }    
    }
    printf("\n");
    for (int i = 1; i <= n; i++)    
    {    
        for (int j = 1; j <= m; j++)    
        {    
            printf("[%d][%d]%d \t",i, j, butterfly[i][j]);  
        }
        printf("\n");
    }
    
    /* Finding Headnode */
    srand(time(0));
    head_node_n = (rand() % (n + 1)) + 0;
    head_node_m = (rand() % (m + 1)) + 0;
    printf("\nThe Head Node: %d\n", butterfly[head_node_n][head_node_m]);

    /* 1st Coloumn */
    for (int i = 1; i <= n; i++)
    {
        path_1[i][1] = butterfly[i][1]; 
        // path_1[i][2] = butterfly[i][2];
        // path_1[i][2] = 0;
        if (i <= m)
        {
            path_1[i][2] = butterfly[i+m][2];
        }
        else
            path_1[i][2] = butterfly[i-m][2];  
    }

    /* 1st Coloumn for full list */
    for (int i = 1; i <= n; i++)
    {
        path_12[i][1] = butterfly[i][1]; 
        path_12[i][2] = butterfly[i][2];
        // path_1[i][2] = 0;
        if (i <= m)
        {
            path_12[i][3] = butterfly[i+m][2];
        }
        else
            path_12[i][3] = butterfly[i-m][2];  
    }

    /* PRINT */
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j < 3; j++)
    //     {
    //         printf("%d\t", path_1[i][j]);
    //     }
    //     printf("\n");
    // }
    // printf("\n");


    /* 2nd Coloumn */
    m = n/2;
    int temp = m/2;
    for (int i = 1; i <= n; i++)
    {
        path_2[i][1] = butterfly[i][2];
        path_2[i][2] = butterfly[i][3];
        // path_2[i][2] = 0;
        if (i <= m)
        {
            if (i <= temp)
            {
                path_2[i][3] = butterfly[i+temp][3];
            }
            else
                path_2[i][3] = butterfly[i-temp][3];
        }
        else
        {
            if (i <= 6)
            {
                path_2[i][3] = butterfly[i+temp][3]; 
            }
            else
            {
                path_2[i][3] = butterfly[i-temp][3];
            }  
        }
    }
    /* PRINT */
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j <= 3; j++)
    //     {
    //         printf("%d\t", path_2[i][j]);
    //     }
    //     printf("\n");
    // }
    // printf("\n");


    /* 3rd coloumn */
    m = m/2;
    for (int i = 1; i <= n; i++)
    {
        path_3[i][1] = butterfly[i][3];
        path_3[i][2] = butterfly[i][4];
        // path_3[i][2] = 0;
        if (i % 2 != 0)
        {
            path_3[i][3] = butterfly[i+1][4];
        }
        else
        {
            path_3[i][3] = butterfly[i-1][4];
        }  
    }
    /* PRINT */
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j <= 3; j++)
    //     {
    //         printf("%d\t", path_3[i][j]);
    //     }
    //     printf("\n");
    // }


    for (int i = 1; i <=n ; i++)
    {
        count = 1;
        for (int j = 1; j < 3; j++)
        {
            final_path[i][count] = path_1[i][j];
            count = count + 1;
        }
        int k = 2;
        for (int j = 1; j <= n; j++)
        {
            if(final_path[i][k] == path_2[j][1])
            {
                int l = 3;
                final_path[i][count] = path_2[j][l];
                count = count + 1;
            }
        }

        k = 3;
        for (int j = 1; j <= n; j++)
        {
            if(final_path[i][k] == path_3[j][1])
            {
                int l = 3;
                final_path[i][count] = path_3[j][l];
                count = count + 1;
            }
        }
    }
    
    // printf("count: %d\n", count);
    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j < count; j++)
    //     {
    //         printf("[%d][%d]%d    ", i, j, final_path[i][j]);
    //     }
    //     printf("\n");
    // }

    /* Creating a 2d array for the full list and storing it in final_path_12 */
    int count_1 = 0;
#if 0
    for (int i = 1; i <=n ; i++)
    {
        count_1 = 1;
        for (int j = 1; j <= 3; j++)
        {
            final_path_1[i][count_1] = path_12[i][j];
            count_1 = count_1 + 1;
        }
        for (int k = 2; k <= 3; k++)
        {
            for (int j = 1; j <= n; j++)
            {
                if(final_path_1[i][k] == path_2[j][1])
                {
                    for (int l = 2; l <=3; l++)
                    {
                        final_path_1[i][count_1] = path_2[j][l];
                        count_1 = count_1 + 1;
                    } 
                }
            }
        }

        for (int k = 4; k <= 5; k++)
        {
            for (int j = 1; j <= n; j++)
            {
                if(final_path_1[i][k] == path_3[j][1])
                {
                    for (int l = 2; l <=3; l++)
                    {
                        final_path_1[i][count_1] = path_3[j][l];
                        count_1 = count_1 + 1;
                    } 
                }
            }
        }
    }

    printf("count: %d\n", count_1);
    for (int i = 1; i <= n; i++)
    {
        for (int j = 1; j < count_1; j++)
        {
            printf("[%d][%d]%d    ",i, j, final_path_1[i][j]);
        }
        printf("\n");
    }
#endif
    // printf("\n");
    // printf("\n");

    // for (int i = 1; i <= n; i++)
    // {
    //     for (int j = 1; j < count; j++)
    //     {
    //         printf("[%d][%d]%d    ", i, j, final_path[i][j]);
    //     }
    //     printf("\n");
    // }
    routing_butterfly(n, count, count_1, n/2, final_path, final_path_1, butterfly, butterfly[head_node_n][head_node_m], decision);
}

/** @fn int node_value_m(int len, char *array)
 * @brief Function helps to find the value of no. of coloumn 
 * @param len Passing the length of the string reading from txt file
 * @param array Passing the array
 */
int node_value_m(int len, char *array)
{
    int count = 0, start = 0;
    for (int k = 3; k < len-1; k++)
    {
        // printf("\n%c", array[k]);
        if (array[k] == ',')
        {
            start = k;
            break;
        }
    }
    for (int k = start+1; k < len-1 ; k++)
    {
        if (array[k] == ')')
        {
            break;
        }
        else
        {
            count = count * 10;
            count = count + (array[k] - '0');
        }
    }
    return count;
}

/** @fn int node_value(int len, char *array)
 * @brief Function helps to find the value of no. of row 
 * @param len Passing the length of the string reading from txt file
 * @param array Passing the array
 */
int node_value(int len, char *array)
{
    int count = 0;
    for (int k = 3; k < len-1; k++)
    {
        // printf("\n%c", array[k]);
        if (array[k] == ')' || array[k] == ',')
        {
            break;
        }
        else
        {
            count = count * 10;
            count = count + (array[k] - '0');
        }
    }
    return count;
}

/** @fn int repetation_steps(char *read_L1, int src_prefix, int dest_prefix, int i, char ch, int decision)
 * @brief 
 * @param char *read_L1
 * @param int src_prefix
 * @param int dest_prefix
 * @param int i
 * @param char ch
 * @param int decision
 */
int repetation_steps(char *read_L1, int src_prefix, int dest_prefix, int i, char ch, int decision)
{
    int n = 0, m = 0, headnode = 0;

    switch (ch)
    {
        case 'C':
            // printf("\n==========Chain==========\n");
            n = node_value(strlen(read_L1), read_L1);
            // printf("Enter the node n");
            // scanf("%d", &n);
            // printf("Id number: %d\n", i);
            // fprintf(out_file, "\nChain Network: Id Number: %d", i);
            headnode = chain_network(n, i, 0, decision);
            // Head_node[i] = headnode;
        break;
        case 'R':
            // printf("\n==========Ring==========\n");
            n = node_value(strlen(read_L1), read_L1);
            // printf("Enter the node n");
            // scanf("%d", &n);
            // printf("Id number: %d\n", i);
            // fprintf(out_file, "\nRing Network: Id Number: %d", i);
            headnode = ring_network(n, i, 0, decision);
            // Head_node[i] = headnode;
        break;
        case 'M':
            // printf("\n==========Mesh==========\n");
            n = node_value(strlen(read_L1), read_L1);
            m = node_value_m(strlen(read_L1), read_L1);
            // printf("Enter the node n and m");
            // scanf("%d \n %d", &n, &m);
            // printf("Id number: %d\n", i);
            headnode = mesh_network(n, m, i, 0, decision);
            // fprintf(out_file, "\nMesh Network: Id Number: %d\n", i);
            // printf("\n The length of the path:%d", len);
            // printf("\nThe PATH:");
            // for (int i = 0; i < len; i++)
            // {
            //     printf("\t%d", path[i]);
            // }
        break;
        case 'F':
            // printf("\n==========Folded Torus==========\n");
            printf("\nFolded Torus\n");
            n = node_value(strlen(read_L1), read_L1);
            m = node_value_m(strlen(read_L1), read_L1);
            // printf("Enter the node n and m");
            // scanf("%d \n %d", &n, &m);
            // printf("Id number: %d\n", i);
            headnode = folded_torus_network(n, m, i, 0, decision);
            // fprintf(out_file, "\nFolded Torus Network: Id Number: %d", i);
        break;     
        case 'H':
            // printf("\n==========Hypercube==========\n");
            printf("The size defined to the Hypercube is 8\n");
            // fprintf(out_file, "\nHypercube Network: Id Number: %d", i);
            // printf("Id number: %d\n", i);
            headnode = hypercube_network(8, i, 0, decision);
        break;
        case 'B':
            printf("\n==========Butterfly==========\n");
            printf("\nPlz Enter k if the configuration is 2^k*2^k\n");
            n = node_value(strlen(read_L1), read_L1);
            // fprintf(out_file, "\nButterfly Network: Id Number: %d", i);
            // printf("Id number: %d\n", i);
            butterfly_network(n, i, decision);
        break;
        default:
        break;    
    }
    return 0;
}

/** @fn void main()
 * @brief 
 *        C for Chain Topology
 *        R for Ring Topology
 *        M for Mesh Topology
 *        F for Folded Torus Topology
 *        H for Hypercube Topology
 *        B for Butterfly Tolpology
 *
 */
void main()
{
    out_file = fopen("output.txt", "w+");
    L0Topology = fopen("L0Topology.txt", "r");

    char read_L0[100], read_L1[200];
    char ch;
    int no_L1_topology = 0, n, m, dest_prefix, src_prefix;

    fgets(read_L0, sizeof(read_L0), L0Topology);
    fclose(L0Topology);

    if (read_L0[1] == 'C' || read_L0[1] == 'R'|| read_L0[1] == 'B' || read_L0[1] == 'H')
    {
        no_L1_topology = node_value(strlen(read_L0), read_L0);
    }
    else
    {
        n = node_value(strlen(read_L0), read_L0);
        m = node_value_m(strlen(read_L0), read_L0);
        // printf("n: %d, m: %d", n, m);
        no_L1_topology = n * m;
    }

    L1Topology = fopen("L1Topology.txt", "r");

    // printf("\nNo_L1_topology: %d", no_L1_topology);

    // printf("\nPlz enter the L1 Network");
    // printf("\n C for Chain\n R for Ring\n M for Mesh\n F for Folded Torus\n H for Hypercube\n B for Butterfly\n");
    // scanf("%c", &ch);
    printf("\n Please Enter the Source and Destination Prefix: ");
    scanf("%d \n %d", &src_prefix, &dest_prefix);

    if(src_prefix == dest_prefix)
    {
        printf("\n From Same Tiles");
        for (int i = 1; i <= no_L1_topology; i++)
        {
            fgets(read_L1, sizeof(read_L1), L0Topology);
            // printf("\n%s", read_L1);
            // printf("\nPlz enter the L1 Network");
            ch = read_L1[1];

            if(i == src_prefix)
            {
                repetation_steps(read_L1, src_prefix, dest_prefix, i, ch, 0);
            }
            else
                continue;
        }
    }
    else
    {
        printf("\n From Different Tiles");
        for (int i = 1; i <= no_L1_topology; i++)
        {
            fgets(read_L1, sizeof(read_L1), L0Topology);
            // printf("\n%s", read_L1);
            // printf("\nPlz enter the L1 Network");
            ch = read_L1[1];
            if(i == src_prefix)
            {

                repetation_steps(read_L1, src_prefix, dest_prefix, i, ch, 1);
            }
            else
                continue;
        }
        fclose(L1Topology);

        printf("\n\nIntermediate Path: ");
        if (src_prefix < dest_prefix)
        {
            for (int i = src_prefix+1; i < dest_prefix; i++)
            {
                printf("\t0_%d", i);
            }
        }

        else if (src_prefix > dest_prefix)
        {
            for (int i = src_prefix-1; i > dest_prefix; i--)
            {
                printf("\t0_%d", i);
            }
        }
        
        L1Topology = fopen("L1Topology.txt", "r");

        printf("\n");
        for (int i = 1; i <= no_L1_topology; i++)
        {
            fgets(read_L1, sizeof(read_L1), L0Topology);
            // printf("\n%s", read_L1);
            // printf("\nPlz enter the L1 Network");
            if(i == dest_prefix)
            {
                ch = read_L1[1];

                repetation_steps(read_L1, src_prefix, dest_prefix, i, ch, 2);
            }
            else
                continue;
        }
    }
}


